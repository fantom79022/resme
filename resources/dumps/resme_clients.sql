create table clients
(
    id         bigint unsigned auto_increment
        primary key,
    user_id    varchar(255)                null,
    chat_id    varchar(255) default ''     null,
    role       varchar(255) default 'user' not null,
    created_at timestamp                   null,
    updated_at timestamp                   null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO resme.clients (id, user_id, chat_id, role, created_at, updated_at) VALUES (1, '73060171', null, 'admin', '2022-07-28 09:39:37', '2022-07-28 09:39:37');
INSERT INTO resme.clients (id, user_id, chat_id, role, created_at, updated_at) VALUES (2, '311962894', null, 'admin', '2022-07-28 09:39:37', '2022-07-28 09:39:37');
INSERT INTO resme.clients (id, user_id, chat_id, role, created_at, updated_at) VALUES (3, '203166081', null, 'admin', '2022-07-28 09:39:37', '2022-07-28 09:39:37');
