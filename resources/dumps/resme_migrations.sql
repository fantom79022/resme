create table migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

INSERT INTO resme.migrations (id, migration, batch) VALUES (1, '2020_01_29_223913_create_table_parrot', 1);
INSERT INTO resme.migrations (id, migration, batch) VALUES (2, '2020_01_30_000928_messages', 1);
INSERT INTO resme.migrations (id, migration, batch) VALUES (3, '2020_01_30_193003_create_events', 1);
INSERT INTO resme.migrations (id, migration, batch) VALUES (4, '2020_02_02_061600_alter_triggers_add_regexp_type', 1);
INSERT INTO resme.migrations (id, migration, batch) VALUES (5, '2020_02_13_191821_create_roles', 1);
