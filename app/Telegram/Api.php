<?php

namespace App\Telegram;

use Telegram\Bot\Objects\Message as MessageObject;

class Api extends \Telegram\Bot\Api
{

    public function sendReaction(array $params): MessageObject
    {
        $response = $this->post('setMessageReaction', $params);

        return new MessageObject($response->getDecodedBody());
    }

}
