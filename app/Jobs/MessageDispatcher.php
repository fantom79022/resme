<?php


namespace App\Jobs;


interface MessageDispatcher
{
    const DISPATCHERS = [
        Reactor::class,
        Administration::class
    ];
}
