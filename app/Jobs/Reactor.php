<?php

namespace App\Jobs;

use App\Helpers\MessageHelper;
use App\Models\Trigger;
use App\Services\EventHandlers\EventHandler;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Objects\Message;

class Reactor implements ShouldQueue, MessageDispatcher
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $message;

    /**
     * @var int
     */
    public $tries = 1;

    /**
     * @var int
     */
    public $timeout = 120;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function handle(Trigger $triggers): void
    {
        //TODO:: handle video/photo/audio with captions
        $trigger = MessageHelper::getMessageContext($this->message);
        $chatId = Arr::get($this->message, 'chat.id');
        $messageId = Arr::get($this->message, 'message_id');
        if (!($trigger && $chatId && $messageId)) {
            Log::error(__('Message and Chat Id are required'));
            return;
        }
        $this->getReaction($triggers, $trigger);
    }

    /**
     * @param Trigger $triggers
     * @param string $trigger
     */
    private function getReaction(Trigger $triggers, string $trigger): void
    {
        $triggers = $triggers->searchTrigger($trigger)->get();
        if (!$trigger) {
            return;
        }
            foreach ($triggers as $trigger) {
                try {
                    $events = $trigger->events;
                    $events = collect($events);
                    while ($events->first()) {
                        $event = $events->first();
                        $handler = $this->initHandler($event);
                        $events = $handler->process($event, $this->message, $events->slice(1), $event);
                    }
                } catch (\Throwable $t) {
                    Log::error($t->getMessage(), $t->getTrace());
                }
        }
    }

    private function initHandler($eventType): EventHandler
    {
        $eventType = Arr::get(EventHandler::TYPES, $eventType->type);
        if ($eventType === null) {
            throw new \LogicException(__('Wrong event Type'));
        }
        return app($eventType);
    }
}
