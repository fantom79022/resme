<?php

namespace App\Jobs;

use App\Models\Client;
use App\Services\Administration\AdminCommand;
use App\Services\Administration\CunningReply;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Telegram\Bot\Objects\Message;


class Administration extends AbstractMessageDispatcher implements ShouldQueue, MessageDispatcher
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const ACCESS_ROLE = 'admin';

    /**
     * @var Message
     */
    protected $message;

    /**
     * @var Collection|Client
     */
    public $clients;

    /**
     * @var int
     */
    public $tries = 1;

    /**
     * @var int
     */
    public $timeout = 120;

    public function __construct(Message $message)
    {
        parent::__construct($message);
    }

    public function handle(): void
    {
        if (!$this->checkAccess([self::ACCESS_ROLE])) {
            return;
        }
        $message = Arr::get($this->message, 'text');
        $tokens = explode(' ', $message, 2);
        $command = Arr::get($tokens, 0);
        $arguments = Arr::get($tokens, 1);
        $chatId = Arr::get($this->message, 'chat.id');
        $messageId = Arr::get($this->message, 'message_id');
        if (!($chatId && $messageId)) {
            Log::error(__('Message and Chat Id are required'));
            return;
        }
        $handlerName = Arr::get(AdminCommand::COMMANDS, $command) ?? CunningReply::class;
        if ($handlerName) {
            /** @var AdminCommand $handler */
            $handler = app($handlerName);
            $handler->process($arguments, $this->message);
        }
    }
}
