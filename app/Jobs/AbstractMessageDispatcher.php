<?php

namespace App\Jobs;

use App\Models\Client;
use Telegram\Bot\Objects\Message;

abstract class AbstractMessageDispatcher implements MessageDispatcher
{
    /**
     * @var Message
     */
    protected $message;

    public $clients;

    public function __construct(Message $message)
    {
        $this->message = $message;
        /** @var Client $clientModel */
        $clientModel = app(Client::class);
        $this->clients = $clientModel->getClients($this->message);
    }

    protected function checkAccess(array $roles): bool
    {
        foreach ($roles as $role) {
            if ($this->clients->contains('role', $role)) {
                return true;
            }
        }
        return false;
    }
}
