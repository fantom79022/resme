<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Config;
use VK\Client\VKApiClient;

class VkHelper
{
    const TYPE_USER = 'user';
    const TYPE_GROUP = 'group';
    const COMMENTS_PER_QUERY = 100;
    const THREAD_ITEMS = 10;



    private static $_vk;

    private static function getVkClient(): VKApiClient
    {
        if (!self::$_vk) {
            self::$_vk = new VKApiClient(Config::get('constants.vk.api.version'));
        }
        return self::$_vk;
    }

    /**
     * we can use only 3 queries per second. Probably should be fixed TODO
     */
    protected static function wait()
    {
        usleep(400000);
    }

    /**
     * @param $id int
     *
     * @return string
     */
    public static function getUserNameById(int $id): string
    {
        $name = '';
        try {
            $user = self::getVkClient()->users()->get(
                Config::get('constants.vk.token'),
                ['user_ids' => $id]
            );
            self::wait();
        } catch (\Exception $e) {}
        if (isset($user[0])) {
            $user = $user[0];
            $name = TextHelper::getUserName($user);
        }
        return $name;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public static function getGroupNameById(int $id): string
    {
        $name = '';
        try {
            $group = self::getVkClient()->groups()->getById(
                Config::get('constants.vk.token'),
                ['group_ids' => $id]
            );
            self::wait();
        } catch (\Exception $e) {}
        if (isset($group[0])) {
            $group = $group[0];
            $name = TextHelper::getGroupName($group);
        }
        return $name;
    }

    /**
     * @param int $albumId
     * @param int $ownerId
     *
     * @return array
     */
    public static function getAlbum(int $albumId, int $ownerId): array
    {
        $album = self::getAlbumInfo($albumId, $ownerId);
        $photos = self::getAlbumPhotos($albumId, $ownerId);
        return [
            'album' => $album,
            'photos' => $photos
        ];
    }

    /**
     * @param int $video
     * @param int $owner
     *
     * @return string
     */
    public static function getVideo(int $video, int $owner): string
    {
        $url = '';
        try {
            $videos = self::getVkClient()->video()->get(
                Config::get('constants.vk.user'),
                [
                    'owner_id' => $owner,
                    'videos' => $owner . '_' . $video,
                    'count' => 1,
                    'offset' => 0,
                    'extended' => 1,
                ]
            );
            self::wait();

            if (isset($videos['items'][0]['player'])) {
                $url = $videos['items'][0]['player'];
                $url = TextHelper::properYoutube($url);
            }
        } catch (\Exception $e) {}
        return $url;
    }

    private static function getAlbumPhotos(int $albumId, int $ownerId): array
    {
        $photos = [];
        try {
            $albumPhotos = self::getVkClient()->photos()->get(
                Config::get('constants.vk.service'),
                [
                    'owner_id' => $ownerId,
                    'album_id' => $albumId,
                ]
            );
            self::wait();

        } catch (\Exception $e) {}
        if (isset($albumPhotos['items'])) {
            $items = $albumPhotos['items'];
            foreach ($items as $item) {
                if(is_array($item['sizes']) ) {
                    $photo = end($item['sizes']);
                    if (isset($photo['url'])) {
                        $photos[] = $photo['url'];
                    }
                }
            }
        }
        return $photos;
    }

    /**
     * @param int $albumId
     * @param int $ownerId
     *
     * @return array
     */
    private static function getAlbumInfo(int $albumId, int $ownerId): array
    {
        $album['title'] = Config::get('constants.album.title');
        $album['description'] = '';
        try {
            $albums = self::getVkClient()->photos()->getAlbums(
                Config::get('constants.vk.service'),
                [
                    'owner_id' => $ownerId,
                    'album_ids' => $albumId,
                ]
            );
            self::wait();

        } catch (\Exception $e) {}
        if (isset($albums['items'][0])) {
            $albumInfo = $albums['items'][0];
            $album['title'] = $albumInfo['title'];
            $album['description'] = $albumInfo['description'];
        }
        return $album;
    }

    /**
     * @param array $post
     * @param array $comments
     *
     * @return array
     */
    public static function getComments(array $post, array $comments = []): array
    {
        $commentsObject = self::getCommentsObject($post['owner_id'], $post['post_id'], $post['id'] ?? 0);
        $profiles = self::getProfilesFromObject($commentsObject);
        $comments = self::getThreadComments($commentsObject, $profiles, $comments);
        return $comments;
    }

    /**
     * @param array $object
     *
     * @return array
     */
    private static function getProfilesFromObject(array $object): array
    {
        $profiles = [];
        foreach ($object['profiles'] as $profile) {
            $user = [];
            $user['name'] = $profile['first_name'] . ' ' . $profile['last_name'];
            $user['id'] = 'https://vk.com/id'. $profile['id'];
            $profiles[$profile['id']] = $user;
        }
        return $profiles;

    }
    /**
     * @param array $object
     * @param array $profiles
     * @param array $comments
     *
     * @return array
     */
    private static function getThreadComments(array $object, array $profiles, array $comments = []): array
    {
        foreach ($object['items'] as $index => $commentObject) {
            $comment = [];
            $comment['author'] = $profiles[$commentObject['from_id']] ?? null;
            $comment['text'] = $commentObject['text'];
            if (isset($commentObject['attachments'])) {
                foreach ($commentObject['attachments'] as $attachment) {
                    if ($attachment['type'] == MessageHelper::ATTACHMENT_PHOTO) {
                        $imageObject = end($attachment[MessageHelper::ATTACHMENT_PHOTO]['sizes']);
                        $comment['photo'][] = $imageObject['url'];
                    }
                }
            }
            unset($object['items'][$index]);
            $comments[] = $comment;
            if (isset($commentObject['thread']['count']) && $commentObject['thread']['count'] > 10) {
                $comments = self::getComments($commentObject, $comments);
            } else if (isset($commentObject['thread']['count']) && $commentObject['thread']['count']){
                $comments = self::getThreadComments($commentObject['thread'], $profiles, $comments);
            }
        }
        return $comments;
    }

    /**
     * @param int $owner
     * @param int $post
     * @param int $comment
     *
     * @return array
     */
    private static function getCommentsObject(int $owner, int $post, int $comment = 0): array
    {
        $object = [];
        $object['items'] = [];
        $object['profiles'] = [];
        $moreComments = true;
        $offset = 0;
        $maxPages = 3;
        $page = 1;
        do {
            try {
                $commentsObject = self::getVkClient()->wall()->getComments(
                    Config::get('constants.vk.user'),
                    [
                        'owner_id' => $owner,
                        'post_id' => $post,
                        'extended' => 1,
                        'offset' => $offset,
                        'comment_id' => $comment,
                        'count' => self::COMMENTS_PER_QUERY,
                        'thread_items_count' => self::THREAD_ITEMS,
                    ]
                );
                self::wait();
                $object['items'] = array_merge($object['items'], $commentsObject['items']);
                $object['profiles'] = array_merge($object['profiles'], $commentsObject['profiles']);
            } catch (\Exception $e) {}
            if (!isset($commentsObject['items']) || !$commentsObject['items'] || $maxPages < $page) {
                $moreComments = false;
            }
            $page++;
            $offset+=self::COMMENTS_PER_QUERY;
        } while ($moreComments);
        return $object;
    }
}