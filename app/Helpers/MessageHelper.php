<?php
namespace App\Helpers;

use App\Services\Telegram;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Telegram\Bot\Objects\Message;

class MessageHelper
{
    const ATTACHMENT_PHOTO  = 'photo';
    const ATTACHMENT_WALL   = 'wall';
    const ATTACHMENT_ALBUM  = 'album';
    const ATTACHMENT_LINK   = 'link';
    const ATTACHMENT_VIDEO  = 'video';
    const ATTACHMENT_POLL   = 'poll';

    const TYPE_OBJECT = 'object';
    const TYPE_WALL = 'wall';
    const TYPE_HISTORY = 'copy_history';

    const PLATFORM_YOUTUBE = 'YouTube';

    private $type;

    public function __construct($type = self::TYPE_OBJECT)
    {
        $this->type = $type;
    }

    /**
     * @param array $message, Telegram message array
     *
     * @return array
     */
    public function getPhotos(array $message): array
    {
        $photos = [];
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_PHOTO) {
                    $photo = $attachment['photo'];
                    $original = end($photo['sizes']);
                    $link = $original['url'];
                    $photos[] = $link;
                }
            }
        }
        return $photos;
    }

    /**
     * @param array $message
     * @param $owner
     *
     * @reference int $owner
     * @return int
     */
    public function getAlbum(array $message, &$owner): int
    {
        $album = 0;
        $owner = 0;
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_ALBUM) {
                    $album = $attachment['album']['id'];
                    $owner = $attachment['album']['owner_id'];
                    break;
                }
            }
        }
        return $album;
    }

    /**
     * @param array $message
     *
     * @return string
     */
    public function getLink(array $message): string
    {
        $link = '';
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_LINK) {
                    if (isset($attachment[self::ATTACHMENT_LINK]['url'])) {
                        $link = $attachment[self::ATTACHMENT_LINK]['url'];
                    }
                    break;
                }
            }
        }
        return $link;
    }

    /**
     * @param array $message
     *
     * @return array
     */
    public function getPoll(array $message): array
    {
        $poll = [];
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_POLL) {
                    if (isset($attachment[self::ATTACHMENT_POLL]['question'])) {
                        $poll['question'] = $attachment[self::ATTACHMENT_POLL]['question'];
                    }
                    if (isset($attachment[self::ATTACHMENT_POLL]['answers'])) {
                        $poll['answers'] = array_map(function ($answer) { return $answer; }, $attachment[self::ATTACHMENT_POLL]['answers']);
                    }
                    break;
                }
            }
        }
        return $poll;
    }

    /**
     * @param array $message
     *
     * @return array
     */
    public function getComments(array $message): array
    {
        $post = [];
        if (isset($message[$this->type]['comments']) && $message[$this->type]['comments']) {
            $post['post_id'] = $message[$this->type]['id'];
            $post['owner_id'] = $message[$this->type]['from_id'];
        }
        return $post;
    }

    /**
     * @param array $message
     * @param int $owner
     *
     * @reference $owner
     * @return int
     */
    public function getVideo(array $message, &$owner): int
    {
        $id = 0;
        $owner = 0;
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_VIDEO) {
                    if (isset($attachment[self::ATTACHMENT_VIDEO]['platform']) && $attachment[self::ATTACHMENT_VIDEO]['platform'] == self::PLATFORM_YOUTUBE) {
                        if (isset($attachment[self::ATTACHMENT_VIDEO]['id'])) {
                            $id = $attachment[self::ATTACHMENT_VIDEO]['id'];
                        }
                        if (isset($attachment[self::ATTACHMENT_VIDEO]['owner_id'])) {
                            $owner = $attachment[self::ATTACHMENT_VIDEO]['owner_id'];
                        }
                    }
                    break;
                }
            }
        }
        return $id;
    }

    /**
     * @param array $message, Telegram message array
     *
     * @return array
     */
    public function getHistory(array $message): array
    {
        $history = [];
        if (isset($message[$this->type]['copy_history'])) {
            $history = $message[$this->type];
        }
        return $history;
    }

    /**
     * @param array $message, Telegram message array
     *
     * @return array
     */
    public function getWall(array $message): array
    {
        $wall = [];
        if (isset($message[$this->type]['attachments'])) {
            $attachments = $message[$this->type]['attachments'];
            foreach ($attachments as $attachment) {
                if ($attachment['type'] == self::ATTACHMENT_WALL) {
                    $wall = $attachment;
                    break;
                }
            }
        }
        return $wall;
    }

    /**
     * @param array $message, Telegram message array
     *
     * @return string
     */
    public function getText($message): string
    {
        $text = '';
        if (isset($message[$this->type]['text'])) {
            $text = $message[$this->type]['text'];
        }
        return $text;
    }

    /**
     * @param $message
     * @param $user
     *
     * @return string
     */
    public function getAuthor($message, &$user): string
    {
        $author = 0;
        $user = 0;
        if (isset($message[$this->type]['from_id'])) {
            $id = $message[$this->type]['from_id'];
            $user = $id > 0; //groups ids are less then 0
            $author = abs($message[$this->type]['from_id']);
        }
        return $author;
    }

    public static function getMessageContext(Message $message): string
    {
        return Arr::get($message, 'text')
            ?? Arr::get($message, 'caption')
            ?? Arr::get($message, 'sticker.file_unique_id')
            ?? Arr::get($message, 'video.file_unique_id')
            ?? Arr::get($message, 'audio.file_unique_id')
            ?? Arr::get($message, 'document.file_unique_id')
            ?? '';
            //?? Arr::get($message, 'photo')[-1]['file_unique_id']; TODO: doesn't work because of telegram returns array of different sizes
    }

}
