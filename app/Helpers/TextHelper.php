<?php
namespace App\Helpers;

class TextHelper
{
    public static function bolder($string): string
    {
        return '<b>' . $string . '</b>';
    }

    /**
     * @param array $user, VK api user array
     *
     * @return string
     */
    public static function getUserName(array $user): string
    {
        return self::bolder($user['last_name'] . ' ' . $user['first_name'] .':');
    }

    /**
     * @param array $group, VK api group array
     *
     * @return string
     */
    public static function getGroupName(array $group): string
    {
        return self::bolder($group['name'] . ':');
    }

    /**
     * @param $url string,
     *
     * @return string
     */
    public static function properYoutube(string $url): string
    {
        return str_replace(['embed/', '?__ref=vk.api'], ['watch?v=', ''], $url);
    }

    /**
     * @param $imageUrl string,
     *
     * @return string
     */
    public static function properImage(string $imageUrl): string
    {
        return rtrim($imageUrl, '?ava=1');
    }

    public static function preparePoll(array $poll) : string
    {
        $text = '';
        if (isset($poll['question']) && isset($poll['answers'])) {
            $text .= '<b>Опрос:</b>' . PHP_EOL;
            $text .= $poll['question'] . PHP_EOL;;
            foreach ($poll['answers'] as $answer) {
                $text .= '<i>•' . $answer['text'] . '</i> <b>' . $answer['rate'] . '% (' . $answer['votes'] . ')</b>' . PHP_EOL;
            }
        }
        return $text;

    }
}