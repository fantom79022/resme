<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Message
 * @mixin Builder
 * @method Client|Collection getClients(\Telegram\Bot\Objects\Message $message)
 * @package App\Models
 */
class Client extends Model
{
    protected $fillable = ['user_id', 'chat_id', 'role'];

    private static $client = [];

    public function scopeGetClients(Builder $builder, \Telegram\Bot\Objects\Message $message): Collection
    {
        $userId = $message->getFrom()->getId();
        $chatId = $message->getChat()->getId();
        if (isset(self::$client[$userId][$chatId])) {
            return self::$client[$userId][$chatId];
        }
        $clients = $builder->where(function (Builder $builder) use ($userId, $chatId) {
            $builder->where('user_id', $userId)
                ->where('chat_id', $chatId);
        })->orWhere(function (Builder $builder) use ($userId) {
            $builder->where('user_id', $userId)
                ->whereNull('chat_id');
        })->orWhere(function (Builder $builder) use ($chatId) {
            $builder->whereNull('user_id')
                ->where('chat_id', $chatId);
        })->get();
        self::$client[$userId][$chatId] = $clients;
        return $clients;
    }
}
