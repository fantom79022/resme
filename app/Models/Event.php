<?php
/**
 * Created by PhpStorm.
 * User: fanto
 * Date: 30.01.2020
 * Time: 2:11
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @mixin Builder
 * @package App\Models
 */
class Event extends Model
{
    const TYPE_RANDOMIZE = 'randomize';
    const TYPE_PHOTO = 'photo';
    const TYPE_STICKER = 'sticker';
    const TYPE_VIDEO = 'video';
    const TYPE_DOCUMENT = 'document';
    const TYPE_ALIAS = 'alias';
    protected $fillable = ['params', 'type', 'parent_event_id'];


    public function trigger()
    {
        return $this->belongsTo(Trigger::class);
    }
}
