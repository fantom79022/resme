<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Message
 * @mixin Builder
 * @method Client|Collection getClients(\Telegram\Bot\Objects\Message $message)
 * @package App\Models
 */
class UserGptRequest extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'counter', 'date', 'user_name'];
    protected $attributes = ['counter' => 0];

    public function getActiveUsers(): array
    {
        $activeUsers = $this->groupBy('user_id')
            ->select([
                'user_id',
                DB::raw('SUM(counter) as count'),
            ])
            ->orderBy('count', 'desc')
            ->get()
            ->toArray();

        $userIds = array_map(fn($user) => $user['user_id'] ,$activeUsers);
        $userNames = $this
            ->whereIn('user_id', $userIds)
            ->orderBy('date', 'DESC')
            ->select('user_name', 'user_id')
            ->get()
            ->toArray();

        $results = [];
        foreach ($activeUsers as $user) {
            $i = 0;
            while ($i < count($userNames)) {
                if ($user['user_id'] === $userNames[$i]['user_id']) {
                    $user['name'] = $userNames[$i]['user_name'];
                    $results[$user['user_id']]['name'] = $userNames[$i]['user_name'];
                    $results[$user['user_id']]['count'] = $user['count'];
                    break;
                }
                $i++;
            }
        }
        return  $results;
    }
}
