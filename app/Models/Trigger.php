<?php
/**
 * Created by PhpStorm.
 * User: fanto
 * Date: 30.01.2020
 * Time: 2:11
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reaction
 * @mixin Builder
 * @method Trigger|Builder searchTrigger(string $trigger)
 * @package App\Models
 */
class Trigger extends Model
{
    protected $fillable = ['trigger', 'regexp'];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function scopeSearchTrigger(Builder $builder, string $trigger)
    {
        $trigger = substr($trigger,0,300);
        $trigger = str_replace(['"', "'"], '', $trigger);
        $builder->where('regexp', 0)
            ->where('trigger', $trigger)
            ->orWhere(function (Builder $builder) use ($trigger) {
                $builder->where('regexp', 1);
                $builder->whereRaw('\'' . $trigger . '\'' . ' REGEXP `trigger`');
            });
        return $builder;
    }

    public function removeTrigger(string $trigger): bool
    {
        $triggerModels = $this->where('trigger', $trigger)->get();
        if ($triggerModels->count() === 0) {
            return false;
        }
        foreach ($triggerModels as $model) {
            $model->events()->delete();
            $model->delete();
        }
        return true;
    }

    public static function isRegexp(string $trigger): array
    {
        $regexp = ($trigger[0] === '/' && $trigger[-1] === '/');
        if ($regexp) {
            return [substr($trigger, 1, -1), true];
        }
        return [$trigger, false];
    }
}
