<?php

namespace App\Console\Commands;

use Amp\Loop;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Telegram\Bot\Api;

class LighterKrem extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'krem';

    public function __construct(private readonly Api $telegram, private readonly Client $client)
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $request = $this->client->get('host.docker.internal:3066');
        $schedule = $request->getBody()->getContents();
        Cache::put('krem', json_decode($schedule, true), 60 * 60 * 24 * 7);
    }
}
