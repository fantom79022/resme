<?php

namespace App\Console\Commands;

use Amp\Loop;
use App\Exceptions\GptLimitExceededException;
use App\Jobs\MessageDispatcher;
use App\Jobs\Reactor;
use App\Models\Message;
use App\Services\AdminLogger\NotifyAdmin;
use App\Services\EventHandlers\Exception\ClownReaction;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use function Amp\Promise\wait;

class TelegramCatcher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catch:telegram';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Catch messages from Telegram';
    /**
     * @var Message
     */
    private $message;

    /**
     * TelegramCatcher constructor.
     * @param Message $message
     */
    public function __construct(Message $message, private NotifyAdmin $notifyAdmin, private ClownReaction $clownReaction)
    {
        parent::__construct();
        $this->message = $message;
    }

    /**
     * Execute the console command.
     *
     * @param Api $telegram
     * @return mixed
     */
    public function handle(Api $telegram): void
    {
        while (true) {
            try {
                $offset = $this->message->latest('id')->first();
                $offset = $offset ? ++$offset->update_id : 0;
                $response = $telegram->getUpdates([
                    'offset' => $offset,
                ]);
                foreach ($response as $message) {
                    $mid = $message->getMessage()->get('message_id');
                    Message::create(['update_id' => $message->getUpdateId(), 'message_id' => $mid]);
                    /** @var Dispatchable $dispatcher */
                    foreach (MessageDispatcher::DISPATCHERS as $dispatcher) {
                        if ($message->getMessage()) {
                            $dispatcher::dispatch($message->getMessage());
                        }
                    }
                }
            } catch (GptLimitExceededException $exception) {
                $this->notifyAdmin->notify($exception->getMessage());
                $this->clownReaction->react($exception->tgMessage);
            } catch (\Throwable $throwable) {
                Log::error(__('Telegram Error: ') . $throwable->getMessage());
                sleep(10);
                continue;
            }
        }
    }
}

