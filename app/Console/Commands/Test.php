<?php

namespace App\Console\Commands;

use Amp\Loop;
use App\Jobs\MessageDispatcher;
use App\Jobs\Reactor;
use App\Models\Message;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use function Amp\Promise\wait;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';
    /**
     * @var OpenAi
     */
    private $openAi;


    public function __construct(OpenAi $openAi)
    {
        parent::__construct();
        $this->openAi = $openAi;
    }

    /**
     * Execute the console command.
     *
     * @param Api $telegram
     * @return mixed
     */
    public function handle(): void
    {
        $a = [
            'Davinci',
            'Davinci,',
            'Vinci',
            'Vinci,',
            'Davi',
            'Davi,',
            'Davi Jhons',
            'Davi Jhons,',
            'Давінчі',
            'Давінчі,',
            'Даві',
            'Даві,',
            'Вінчі',
            'Вінчі,',
            'Дейві Джонс',
            'Дейві Джонс,',
            'Деві Джонс',
            'Деві Джонс,',

        ];

        foreach ($a as $name) {
            echo $name . '|';
        }
    }
}

