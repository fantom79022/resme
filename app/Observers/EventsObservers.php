<?php

namespace App\Observers;

use App\Models\Event;

class EventsObservers
{
    public function creating(Event $event)
    {
        if (in_array($event->type, [Event::TYPE_PHOTO, Event::TYPE_STICKER, Event::TYPE_VIDEO, Event::TYPE_DOCUMENT])) {
            //todo:: save file somewhere;
        }
    }

}
