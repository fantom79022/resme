<?php

namespace App\Exceptions;

use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class GptLimitExceededException extends \RuntimeException
{
    public function __construct(public Message $tgMessage, private int $counter)
    {
        $this->message = "User: @{$this->tgMessage->from->username} ({$this->tgMessage->from->id}) has reached the gpt limit with $this->counter messages";
        parent::__construct();
    }
}
