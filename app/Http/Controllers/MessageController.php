<?php

namespace App\Http\Controllers;

use App\Jobs\Resender;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class MessageController extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function get(Request $request)
    {
        $vkMessage = $request->toArray();
        dispatch(new Resender($vkMessage));
        die('ok');
    }
}
