<?php

namespace App\Services\Scheduler;

use App\Telegram\Api;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class Lighter
{
    const ON = 'Планується підключення за півгодини';
    const OFF = 'Планується відключення за півгодини';
    const MAYBE = 'Можливе підключення за півгодини';

    private array $scheduler;
    private Carbon $time;

    public function __construct(private readonly Api $telegram)
    {
    }
    public function execute(int $group = 5, $type = 'krem'): void
    {
        $schedule = Cache::get($type);
        $this->scheduler =  $schedule['data'][$group];

        $this->time = Carbon::now()->setTimezone('Europe/Kyiv');
        $currentHour = $this->time->format('H');
        $currentHour++;

        $lightNow = $this->getLight($currentHour);
        $lightNextHour = $this->getLight($currentHour+1);


        if (in_array($lightNow, ['yes', 'maybe']) && $lightNextHour === 'no') {
            $this->notify(self::OFF);
        }
        if (in_array($lightNow, ['no', 'maybe']) && $lightNextHour === 'yes') {
            $this->notify(self::ON);
        }
        if ($lightNow === 'no' && $lightNextHour === 'maybe') {
            $this->notify(self::MAYBE);
        }
    }

    private function getLight($hour)
    {
        $dayOfWeek = $this->time->dayOfWeek;
        if ($hour === 25) {
            $dayOfWeek++;
            if ($dayOfWeek === 8) {
                $dayOfWeek = 1;
            }
            $hour = 1;
        }
        return $this->scheduler[$dayOfWeek][$hour];
    }

    private function notify(string $type)
    {
        $this->telegram->sendMessage([
            'chat_id' => 73060171,
            'text' => $type,
        ]);
    }
}
