<?php

namespace App\Services;


use App\Helpers\MessageHelper;
use App\Helpers\TextHelper;
use App\Helpers\VkHelper;


class Message
{

    private $_telegram;
    private $sent;

    private $show_comments;

    const SEND_COMMENTS_COMMAND = 'c';

    public function __construct()
    {
        $this->sent = [];
        $this->show_comments = false;
    }

    /**
     * @return Telegram
     */
    private function getTelegramService(): Telegram
    {
        if (!$this->_telegram) {
            $this->_telegram = new Telegram();
        }
        return $this->_telegram;
    }

    /**
     * @param array $message
     * @param string $type object|wall
     *
     * @return void
     */
    public function process(array $message, string $type = MessageHelper::TYPE_OBJECT): void
    {
        $messageHelper = new MessageHelper($type);

        $author = $messageHelper->getAuthor($message, $user);
        $text = $messageHelper->getText($message);
        $photos = $messageHelper->getPhotos($message);
        $album = $messageHelper->getAlbum($message, $owner);
        $video = $messageHelper->getVideo($message, $videoOwner);
        $link = $messageHelper->getLink($message);
        $poll = $messageHelper->getPoll($message);
        $walls = $messageHelper->getWall($message);
        $history = $messageHelper->getHistory($message);

        if ($type == MessageHelper::TYPE_OBJECT) {
            $text = $this->processCommands($text);
        }
        $this->processAuthor($author, $type, $user);
        $this->processText($text);
        $this->processMedia($photos);
        $this->processLink($link);
        $this->processVideo($video, $videoOwner);
        $this->processPoll($poll);

        if($this->show_comments) {
            $post = $messageHelper->getComments($message);
            $this->processComments($post);
        }
        $this->processAlbum($album, $owner);
        $this->processWall($walls);
        $this->processHistory($history);
    }

    /**
     * @param array $media
     *
     * @return void
     */
    private function processMedia(array $media): void
    {
        if (!$media) {
            return;
        }
        if (count($media) > 1) {
            $this->getTelegramService()->sendMediaGroup($media);
        } else {
            $this->getTelegramService()->sendPhoto(array_shift($media));
        }
    }

    /**
     * @param string $text
     *
     * @return string|null
     */
    private function processCommands(string $text): string
    {
        if (!$text) {
            $text = '';
        }
        switch ($text) {
            case self::SEND_COMMENTS_COMMAND:
                $this->show_comments = true;
                $text = '';
        }
        return $text;
    }

    /**
     * @param string $text
     *
     * @return void
     */
    private function processText(string $text): void
    {
        if (!$text) {
            return;
        }
        $this->getTelegramService()->sendMessage($text);
    }

    /**
     * @param string $url
     *
     * @return void
     */
    private function processLink(string $url): void
    {
        if (!$url) {
            return;
        }
        if (!in_array($url, $this->sent)) {
            $this->sent[] = $url;
            $this->processText($url);
        }
    }

    /**
     * @param $wall
     *
     * return void
     */
    private function processWall(array $wall): void
    {
        if (!$wall) {
            return;
        }
        $this->process($wall, MessageHelper::TYPE_WALL);
    }

    /**
     * @param int $album
     * @param int $owner
     *
     * @return void
     */
    private function processAlbum(int $album, int $owner): void
    {
        if (!$album) {
            return;
        }
        $album = VkHelper::getAlbum($album, $owner);
        $telegraph = new Telegraph();
        $albumUrl =  $telegraph->createAlbum($album);
        if ($albumUrl) {
            $this->getTelegramService()->sendMessage($albumUrl);
        }
    }

    /**
     * @param int $video
     * @param int $owner
     *
     * @return void
     */
    private function processVideo(int $video, int $owner): void
    {
        if (!$video) {
            return;
        }
        $video = VkHelper::getVideo($video, $owner);
        if (!in_array($video, $this->sent)) {
            $this->sent[] = $video;
            $this->processText($video);
        }
    }

    /**
     * @param array $poll
     *
     * @return void
     */
    private function processPoll(array $poll): void
    {
        if (!$poll) {
            return;
        }
        $this->processText(TextHelper::preparePoll($poll));
    }
    /**
     * @param array $post
     *
     * @return void
     */
    private function processComments(array $post): void
    {
        if (!$post) {
            return;
        }
        $comments = VkHelper::getComments($post);
        if ($comments) {
            $telegraph = new Telegraph();
            $url =  $telegraph->commentsPage($comments);
            $this->processText($url);
        }
    }

    /**
     * @param $histories
     *
     * @return void
     */
    private function processHistory(array $histories): void
    {
        if (!$histories || !isset($histories[MessageHelper::TYPE_HISTORY])) {
            return;
        }
        foreach ($histories[MessageHelper::TYPE_HISTORY] as $history) {
            $history = [MessageHelper::TYPE_HISTORY => $history];
            $this->process($history, MessageHelper::TYPE_HISTORY);
        }
    }

    /**
     * @param int $author
     * @param string $type
     * @param bool $user
     *
     * @return void
     */
    private function processAuthor(int $author, string $type, bool $user): void
    {
        switch ($type) {
            case MessageHelper::TYPE_WALL :
                $this->getTelegramService()->sendMessage(VkHelper::getGroupNameById($author));
                break;
            case MessageHelper::TYPE_OBJECT:
                $this->getTelegramService()->sendMessage(VkHelper::getUserNameById($author));
                break;
            case MessageHelper::TYPE_HISTORY:
                if ($user) {
                    $this->getTelegramService()->sendMessage(VkHelper::getUserNameById($author));
                } else {
                    $this->getTelegramService()->sendMessage(VkHelper::getGroupNameById($author));
                }
                break;
            default :
                break;
        }
    }

}