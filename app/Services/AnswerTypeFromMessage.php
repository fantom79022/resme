<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Objects\Message;

class AnswerTypeFromMessage
{
    const TYPES = [
        'blank' => 'text',
        'photo' => 'photo',
        'text' => 'text',
        'sticker' => 'sticker',
        'document' => 'document',
        'video' => 'video',
        'audio' => 'audio',
    ];

    public function getAnswerAndTypeFromMessage(Message $replyMessage, $fileId = false): array
    {
        $answer = null;
        $messageType = null;
        foreach (self::TYPES as $eventName => $type) {
            if ($replyMessage->get($type)) {
                $answer = $this->$eventName($replyMessage, $fileId);
                if ($answer) {
                    $messageType = $eventName;
                    break;
                }
            };
        }
        return [$answer, $messageType];
    }

    private function photo(Message $replyMessage)
    {
        /** @var Collection $sizes */
        $sizes = $replyMessage->getPhoto();
        $photo = $sizes->last();

        return Arr::get($photo, 'file_id');
    }

    private function blank(Message $replyMessage)
    {
        $text =  $replyMessage->getText();
        if ($text === '/blank') {
            return true;
        }
        return null;
    }

    private function text(Message $replyMessage)
    {
        return $replyMessage->getText();
    }

    private function sticker(Message $replyMessage, bool $fileId)
    {
        return $fileId ? Arr::get($replyMessage, 'sticker.file_unique_id') : $replyMessage->getSticker()->getFileId();
    }

    private function document(Message $replyMessage)
    {
        return $replyMessage->getDocument()->getFileUniqueId();
    }

    private function video(Message $replyMessage)
    {
        return $replyMessage->getVideo()->getFileUniqueId();
    }

    private function audio(Message $replyMessage)
    {
        return $replyMessage->getAudio()->getFileUniqueId();
    }

}
