<?php
/**
 * Created by PhpStorm.
 * User: fanto
 * Date: 14.03.2019
 * Time: 11:50
 */

namespace App\Services;



use Illuminate\Support\Facades\Config;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use React\EventLoop\Factory;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMediaGroup;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Methods\SendPhoto;
use unreal4u\TelegramAPI\Telegram\Types\Custom\InlineKeyboardButtonArray;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\InputMedia\Photo;
use unreal4u\TelegramAPI\TgLog;

class Telegram
{
    const MAX_LENGTH = 4096;



    public static function debug($message):void
    {
        $message = \GuzzleHttp\json_encode($message);
        $t = new self();
        $t->sendMessage($message);
        die('ok');
    }

    /**
     * @param string $message, Message string
     *
     * @return void
     */
    public function sendMessage(string $message): void
    {
        $parts = $this->prepareMessage($message);
        $loop = Factory::create();
        $tgLog = new TgLog(Config::get('constants.telegram.token'), new HttpClientRequestHandler($loop));
        foreach ($parts as $part) {
            $sendMessage = new SendMessage();
            $sendMessage->chat_id = Config::get('constants.telegram.im');
            $sendMessage->text = $part;
            $sendMessage->parse_mode = 'HTML';
            $tgLog->performApiRequest($sendMessage);
            $loop->run();
        }
    }

    /**
     * @param array $photos, Array of image urls
     *
     * @return void
     */
    public function sendMediaGroup(array $photos): void
    {
        $loop = Factory::create();
        $tgLog = new TgLog(Config::get('constants.telegram.token'), new HttpClientRequestHandler($loop));
        $sendMediaGroup = new SendMediaGroup();
        $sendMediaGroup->chat_id = Config::get('constants.telegram.im');
        foreach ($photos as $photoLocation) {
            $inputMedia = new Photo();
            $inputMedia->media = $photoLocation;
            $sendMediaGroup->media[] = $inputMedia;
        }
        $tgLog->performApiRequest($sendMediaGroup);
        $loop->run();
    }

    /**
     * @param string $photo, Image url
     *
     * return void
     */
    public function sendPhoto(string $photo): void
    {
        $loop = Factory::create();
        $tgLog = new TgLog(Config::get('constants.telegram.token'), new HttpClientRequestHandler($loop));
        $sendPhoto = new SendPhoto();
        $sendPhoto->chat_id = Config::get('constants.telegram.im');
        $sendPhoto->photo = $photo;
        $tgLog->performApiRequest($sendPhoto);
        $loop->run();
    }

    /**
     * @param string $url
     * @param string $title
     *
     * @return void
     */
    public function sendButtonUrl(string $url, string $title): void
    {
        $loop = Factory::create();
        $tgLog = new TgLog(Config::get('constants.telegram.token'), new HttpClientRequestHandler($loop));
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = Config::get('constants.telegram.im');
        $inlineKeyboard = new Markup([
            'inline_keyboard' => [
                [
                    ['text' => $title, 'url' => $url],
                ],
            ]
        ]);
        $sendMessage->text = Config::get('constants.link.text');
        $sendMessage->parse_mode = 'Markdown';
        $sendMessage->reply_markup = $inlineKeyboard;
        $tgLog->performApiRequest($sendMessage);
        $loop->run();
    }

    /**
     * @param string $message
     *
     * @return array
     */
    protected function prepareMessage(string $message): array
    {
        if (strlen($message) > self::MAX_LENGTH) {
            $message = str_split($message, self::MAX_LENGTH);
        } else {
            $message = [$message];
        }
        return $message;
    }

}
