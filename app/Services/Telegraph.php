<?php
/**
 * Created by PhpStorm.
 * User: fanto
 * Date: 14.03.2019
 * Time: 11:50
 */

namespace App\Services;



use Illuminate\Support\Facades\Config;
use SSitdikov\TelegraphAPI\Client\TelegraphClient;
use SSitdikov\TelegraphAPI\Request\CreatePageRequest;
use SSitdikov\TelegraphAPI\Request\GetAccountInfoRequest;
use SSitdikov\TelegraphAPI\Type\Account;
use SSitdikov\TelegraphAPI\Type\ContentType\ImageType;
use SSitdikov\TelegraphAPI\Type\ContentType\LinkType;
use SSitdikov\TelegraphAPI\Type\ContentType\ParagraphType;
use SSitdikov\TelegraphAPI\Type\Page;

class Telegraph
{
    public function createAlbum(array $album = []): string
    {
        $url = '';
        if (isset($album['photos']) && $album['photos']) {
            $telegraph = new TelegraphClient();

            $account = new Account();
            $account->setAccessToken(Config::get('constants.telegraph.token'));
            $account = $telegraph->getAccountInfo(new GetAccountInfoRequest($account));

            $page = new Page();
            $page->setTitle($album['album']['title']);
            $page->setAuthorName($account->getAuthorName());

            $content = [];

            if ($album['album']['description']) {
                $paragraph = new ParagraphType();
                $paragraph->setText($album['album']['description']);
                $content[] = $paragraph;
            }

            foreach ($album['photos'] as $index => $photo) {
                $image = new ImageType();
                $image->setSrc($photo);
                $content[] = $image;
            }

            $page->setContent($content);
            $page = $telegraph->createPage(new CreatePageRequest($page, $account));
            $url = $page->getUrl();
        }
        return $url;
    }

    public function commentsPage(array $comments = []): string
    {
        $telegraph = new TelegraphClient();

        $account = new Account();
        $account->setAccessToken(Config::get('constants.telegraph.token'));
        $account = $telegraph->getAccountInfo(new GetAccountInfoRequest($account));

        $page = new Page();
        $page->setTitle(Config::get('constants.telegraph.comments'));
        $page->setAuthorName($account->getAuthorName());
        $content = [];
        foreach ($comments as $comment) {
            if (isset($comment['author'])) {
                $author = new LinkType();
                $author->setText($comment['author']['name'] . ': ');
                $author->setHref($comment['author']['id']);
                $content[] = $author;
            }
            if (!$comment['text']) {
                $comment['text'] = ' ';
            }
            $paragraph = new ParagraphType();
            $paragraph->setText($comment['text'] ?? 'Картинка:');
            $content[] = $paragraph;
            if (isset($comment['photo'])) {
                foreach ($comment['photo'] as $photo) {
                    $image = new ImageType();
                    $image->setSrc($photo);
                    $content[] = $image;
                }
            }
        }
        $page->setContent($content);
        $page = $telegraph->createPage(new CreatePageRequest($page, $account));
        $url = $page->getUrl();
        return $url;
    }
}