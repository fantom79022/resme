<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Loss implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }

    /**
     * |    |I
     * ||   |_
     */
    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        return $events->random(1);
    }
}
