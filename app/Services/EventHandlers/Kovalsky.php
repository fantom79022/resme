<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Kovalsky extends AbstractGptEvent implements EventHandler
{
    const POSITIVE_EMOJIS = [
        '😀',
        '😃',
        '😁',
        '😆',
        '🙂',
        '🙃',
        '😊',
    ];

    const NEGATIVE_EMOJIS = [
        '🤢',
        '🤮',
        '😭',
        '🤡'
    ];

    /**
     * @var Api
     */
    private $telegram;
    private OpenAi $openAi;

    public function __construct(Api $telegram, OpenAi $openAi)
    {
        $this->telegram = $telegram;
        $this->openAi = $openAi;
    }

    public function processGPT(\App\Models\Event $event, Message $message, Collection $events): Collection
    {

        $promt = $message->getReplyToMessage()->text;
        dump($promt);

        try {
            $moderation = $this->openAi->moderation([
                'input' => $promt,
                'model' => 'omni-moderation-latest'
            ]);
            $moderation = json_decode($moderation);

            $result = $moderation->results[0];
            $response = "Аналіз __повідомлення__ на токсичність. Результати:" . PHP_EOL;
            foreach ($result->category_scores as $category => $score) {
                $categoryScore = number_format($score*100,2,'.','');
                $analysisResult = $result->categories->$category ? self::NEGATIVE_EMOJIS[rand(0, count(self::NEGATIVE_EMOJIS)-1)] : self::POSITIVE_EMOJIS[rand(0, count(self::POSITIVE_EMOJIS)-1)];
                $categoryScore = $categoryScore == '0.00' ? '0' : $categoryScore;
                $response.= Str::ucfirst($category)  . ': ' . $analysisResult. ' ('.$categoryScore  . '%' .')' . PHP_EOL;
            }


            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => $response,
                'reply_to_message_id' => $message->getMessageId(),
                'parse_mode' => 'markdown'
            ]);
        } catch (\Throwable $t) {
            dump($t->getMessage());
        }

        return $events;
    }
}
