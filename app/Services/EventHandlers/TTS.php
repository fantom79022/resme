<?php

namespace App\Services\EventHandlers;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Objects\Message;

class TTS implements EventHandler
{

    const VOICES = [
        "en_us_001",
        "en_us_006",
        "en_us_007",
        "en_us_009",
        "en_us_010",
        "en_uk_001",
        "en_uk_003",
        "en_au_001",
        "en_au_002",
        "fr_001",
        "fr_002",
        "de_001",
        "de_002",
        "es_002",
        "es_mx_002",
        "es_male_m3",
        "es_female_f6",
        "es_female_fp1",
        "es_mx_female_supermom",
        "br_003",
        "br_004",
        "br_005",
        "id_001",
        "jp_001",
        "jp_003",
        "jp_005",
        "jp_006",
        "kr_002",
        "kr_004",
        "kr_003",
        "en_us_ghostface",
        "en_us_chewbacca",
        "en_us_c3po",
        "en_us_stitch",
        "en_us_stormtrooper",
        "en_us_rocket",
        "en_female_f08_salut_damour",
        "en_male_m03_lobby",
        "en_male_m03_sunshine_soon",
        "en_female_f08_warmy_breeze",
        "en_female_ht_f08_glorious",
        "en_male_sing_funny_it_goes_up",
        "en_male_m2_xhxs_m03_silly",
        "en_female_ht_f08_wonderful_world"  ,
    ];


    /**
     * @var Api
     */
    private $telegram;
    private ClientInterface $client;

    public function __construct(\App\Telegram\Api $telegram, Client $client)
    {
        $this->telegram = $telegram;
        $this->client = $client;
    }

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $replyMessage = $message->getReplyToMessage();
        if (!$replyMessage) {
            return $events;
        }

        $params = $event->params;
        if (!$params) {
            $command = $message->getText();
            $voice = str_replace(['tts',], '', $command);
            $voice = trim($voice);
        } else {
            $voice = $params;
        }
        /** @var Message $replyMessage */
        $textToSpeech = $this->transliterate($replyMessage->getText());
        if (strlen($textToSpeech)>=300) {
            $this->replyEmoji($message,'🗿');
            return $events;
        }
        if (!$voice) {
            $voice = 'en_us_001';
        }
        try {
            $response = $this->client->post('https://tiktok-tts.weilbyte.dev/api/generate', [
                'json' => [
                    'text' => $this->transliterate($replyMessage->getText()),
                    'voice' => $voice ?? 'en_us_001',
                    'base64' => false
                ],
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]);
            $tmpName = '/tmp/' . sha1(uniqid(mt_rand(), true)) . '.mp3';
            file_put_contents($tmpName, $response->getBody()->getContents());
            $this->telegram->sendVoice([
                'chat_id' => $message->getChat()->getId(),
                'voice' => InputFile::create($tmpName, 'Важливе повідомлення'),
                'reply_to_message_id' => $message->getMessageId(),
            ]);

        } catch (\Throwable $t) {
            Log::error($t->getMessage(), $t->getTrace());
            $this->replyEmoji($message, '🤡');
        }


        return $events;
    }

    private function transliterate(string $text): string
    {
        $cyr = [
            'ж',  'ч',  'щ',   'ш',  'ю',  'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'і', 'ї', 'ґ', 'є',
            'Ж',  'Ч',  'Щ',   'Ш',  'Ю',  'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я', 'І', 'Ї', 'Ґ', 'Є'
        ];
        $lat = [
            'zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'h', 'd', 'e', 'z', 'y', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'kh', 'ts', 'y', '', 'ya', 'i', 'yi', 'g', 'ie',
            'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'H', 'D', 'E', 'Z', 'Y', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'KH', 'ts', 'Y', '', 'ya', 'i', 'yi', 'g', 'ie'
        ];
        return str_replace($cyr, $lat, $text);
    }

    private function replyEmoji(Message $message, string $emoji): void
    {
        $this->telegram->sendReaction([
            'chat_id' => $message->getChat()->getId(),
            'reaction' => '[{"type": "emoji", "emoji": "'.$emoji.'"}]',
            "big" => true,
            'message_id' => $message->getMessageId()
        ]);

    }
}

