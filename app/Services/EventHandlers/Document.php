<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Document implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }


    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        $this->telegram->sendDocument([
            'chat_id' => $message->getChat()->getId(),
            'document' => $params,
            'reply_to_message_id' => $message->getMessageId(),
        ]);
        return $events;
    }
}
