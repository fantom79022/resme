<?php

namespace App\Services\EventHandlers;

use App\Exceptions\GptLimitExceededException;
use App\Models\Client;
use App\Models\UserGptRequest;
use Illuminate\Support\Collection;
use Telegram\Bot\Objects\Message;

abstract class AbstractGptEvent implements EventHandler
{
    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $this->handleGptRequest($message);
        return $this->processGPT($event, $message, $events);
    }

    protected function incrementCounter(UserGptRequest $request)
    {
        $request->counter++;
    }

    private function handleGptRequest(Message $message)
    {
        $adminList = Client::all()->toArray();
        $adminIds = array_column($adminList, 'user_id');
        if (in_array($message->from->id, $adminIds)) {
            return;
        }
        $request = UserGptRequest::firstOrNew(
            ['user_id' => $message->from->id, 'date' => now()->format('Y-m-d')],
            ['user_name' => $message->from->username]
        );
        $this->incrementCounter($request);
        $request->save();

        if ($request->counter > config('telegram.gpt.user_limit')) {
            throw new GptLimitExceededException($message, $request->counter);
        }
    }
    abstract protected function processGPT(\App\Models\Event $event, Message $message, Collection $events): Collection;
}
