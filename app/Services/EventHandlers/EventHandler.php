<?php

namespace App\Services\EventHandlers;

use App\Services\EventHandlers\Administration\AddLoss;
use Illuminate\Support\Collection;
use PhpParser\Comment\Doc;
use Telegram\Bot\Objects\Message;

interface EventHandler
{
    const TYPES = [
        'text' => Text::class,
        'randomize' => Randomize::class,
        'random' => Randomize::class,
        'sticker' => Sticker::class,
        'timeout' => Timeout::class,
        'photo' => Photo::class,
        'loss' => Loss::class,
        'blank' => Blank::class,
        'add-loss' => AddLoss::class,
        'video' => Document::class,
        'gif' => Document::class,
        'document' => Document::class,
        'audio' => Document::class,
        'davinci' => Davinci::class,
        'odin' => Odin::class,
        'curie' => Curie::class,
        'kovalsky' => Kovalsky::class,
        'dali' => Dali::class,
        'reaction' => Reaction::class,
        'reply-to-bot' => ReplyToBot::class,
        'tts' => TTS::class,
        'alias' => Alias::class,
    ];

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection;
}
