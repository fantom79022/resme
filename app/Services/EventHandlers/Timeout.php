<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Objects\Message;

class Timeout implements EventHandler
{
    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        sleep($params);
        return $events;
    }
}
