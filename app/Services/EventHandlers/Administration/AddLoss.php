<?php

namespace App\Services\EventHandlers\Administration;

use App\Models\Trigger;
use App\Services\EventHandlers\EventHandler;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class AddLoss implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $lossTrigger;

    public function __construct(Api $telegram, Trigger $lossTrigger)
    {
        $this->telegram = $telegram;
        $this->lossTrigger = $lossTrigger;
    }

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        /** @var Collection $sizes */
        $sizes = $message->getReplyToMessage()->getPhoto();

        $photo = $sizes->last();
        $photoId = Arr::get($photo, 'file_id');
        if (!$photoId) {
            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __('Photo not found'),
                'reply_to_message_id' => $message->getMessageId()
            ]);
            return $events;
        }

        $lossEvents = $this->lossTrigger->events();
        $lossEvents->where('params', $photoId)->firstOrCreate([
            'params' => $photoId,
            'type' => 'photo'
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => $params,
            'reply_to_message_id' => $message->getMessageId()
        ]);
        return $events;
    }
}
