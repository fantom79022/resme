<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Objects\Message;

class Sticker implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }


    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        try {
            $this->telegram->sendSticker([
                'chat_id' => $message->getChat()->getId(),
                'sticker' => $params,
                'reply_to_message_id' => $message->getMessageId()
            ]);
        } catch (TelegramSDKException $e) {
            Log::error($e->getMessage());
        }
        return $events;
    }
}
