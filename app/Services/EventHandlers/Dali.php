<?php

namespace App\Services\EventHandlers;

use App\Models\UserGptRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Objects\Message;

class Dali extends AbstractGptEvent implements EventHandler
{
    const DALI_NAMES = [
        'Dali, draw',
        'Dali',
        'Далі, намалюй',
        'Далі',
        'Сальвадор',
        'Salvador',
    ];
    /**
     * @var Api
     */
    private $telegram;
    private OpenAi $openAi;

    public function __construct(Api $telegram, OpenAi $openAi)
    {
        $this->telegram = $telegram;
        $this->openAi = $openAi;
    }

    protected function incrementCounter(UserGptRequest $request)
    {
        $request->counter+=100;
    }

    public function processGPT(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $promt = $message->text;
        $promt = str_replace(self::DALI_NAMES, '', $promt);
        $promt = trim($promt, ', ');

        $answer = $this->generateNewPhoto($promt);
        dump($answer);
        $photo = \Telegram\Bot\FileUpload\InputFile::create($answer->data[0]->url, $answer->created);
        $this->telegram->sendPhoto([
            'chat_id' => $message->getChat()->getId(),
            'photo' => $photo,
            'reply_to_message_id' => $message->getMessageId()
        ]);
        return $events;
    }

    private function generateNewPhoto(string $promt)
    {
        $answer = $this->openAi->image([
            "prompt" => $promt,
            "n" => 1,
            "size" => "1024x1024",
            "response_format" => "url",
            "model" => "dall-e-3",
            "style" => "vivid"
        ]);
        return json_decode($answer);
    }
}
