<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Objects\Message;

class Photo implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }


    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        try {
            $this->telegram->sendPhoto([
                'chat_id' => $message->getChat()->getId(),
                'photo' => $params,
                'reply_to_message_id' => $message->getMessageId()
            ]);
        } catch (TelegramSDKException $e) {
            Log::error($e->getMessage());
        }
        return $events;
    }
}
