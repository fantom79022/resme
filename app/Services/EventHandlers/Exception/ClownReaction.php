<?php

namespace App\Services\EventHandlers\Exception;

use App\Telegram\Api;
use Telegram\Bot\Objects\Message;

class ClownReaction
{
    public function __construct(private Api $telegram)
    {
    }
    public function react(Message $message)
    {
        $this->telegram->sendReaction([
            'chat_id' => $message->getChat()->getId(),
            'reaction' => '[{"type": "emoji", "emoji": "🤡"}]',
            "big" => true,
            'message_id' => $message->getMessageId()
        ]);
    }
}
