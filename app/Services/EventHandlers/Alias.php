<?php

namespace App\Services\EventHandlers;

use App\Models\Trigger;
use App\Services\Administration\AdminCommand;
use App\Services\Administration\CunningReply;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Alias implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(Api $telegram, private readonly Trigger $trigger)
    {
        $this->telegram = $telegram;
    }

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $events = $events->concat($this->handleReactor($event->params));
        $this->handleAdmin($event->params, $message);
        return $events;
    }

    private function handleReactor(string $alias): Collection
    {
        $triggers = $this->trigger->searchTrigger($alias)->get();
        if (!$triggers) {
            return new Collection();
        }
        $collection = new Collection();
        foreach ($triggers as $trigger) {
            $collection = $collection->concat($trigger->events);
        }
        return $collection;

    }
    private function handleAdmin(string $alias, Message $message): void
    {
        $tokens = explode(' ', $alias, 2);
        $command = Arr::get($tokens, 0);
        $arguments = Arr::get($tokens, 1);
        $handlerName = Arr::get(AdminCommand::COMMANDS, $command);
        if (!$handlerName) {
            return;
        }
        $handler = app($handlerName);
        $handler->process($arguments, $message);
    }
}
