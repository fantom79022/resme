<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class ReplyToBot implements EventHandler
{
    const MAX_IMAGE_PIXELS = 1048576;

    const FILE_URL = 'https://api.telegram.org/file/bot<token>/<file_path>';

    const DAVINCI_NAMES = [
        'Davinci',
        'Vinci',
        'Davi',
        'Davi Jhons',
        'Давінчі',
        'Даві',
        'Вінчі',
        'Дейві Джонс',
        'Деві Джонс',
        'бот',
        'вінчі'
    ];
    /**
     * @var Api
     */
    private $telegram;
    private OpenAi $openAi;

    private array $systemMessage;
    private Davinci $davinci;

    public function __construct(Api $telegram, OpenAi $openAi, Davinci $davinci)
    {
        $this->telegram = $telegram;
        $this->openAi = $openAi;
        $this->davinci = $davinci;
    }

    public function process(\App\Models\Event $event, Message $tg, Collection $events): Collection
    {
        if ($this->replyToBot($tg->replyToMessage?->from?->id)) {
            $this->davinci->process($event, $tg, $events);
        }
        return $events;
    }

    private function replyToBot(?int $authorId): bool
    {
        $token = config('telegram.bots.mybot.token');
        $id = substr($token, 0, strpos(config('telegram.bots.mybot.token'), ':'));
        return (int) $id === $authorId;
    }
}
