<?php

namespace App\Services\EventHandlers;

use App\Exceptions\GptLimitExceededException;
use App\Models\UserGptRequest;
use App\Services\Administration\AdminCommand;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\User;

class Odin extends AbstractGptEvent implements EventHandler
{
    const MAX_IMAGE_PIXELS = 1048576;

    const FILE_URL = 'https://api.telegram.org/file/bot<token>/<file_path>';

    const DAVINCI_NAMES = [
        'Davinci',
        'Vinci',
        'Davi',
        'Davi Jhons',
        'Давінчі',
        'Даві',
        'Вінчі',
        'Дейві Джонс',
        'Деві Джонс',
        'бот',
        'вінчі'
    ];
    /**
     * @var Api
     */
    private $telegram;
    private OpenAi $openAi;

    private array $systemMessage;

    public function __construct(Api $telegram, OpenAi $openAi)
    {
        $this->telegram = $telegram;
        $this->openAi = $openAi;
    }

    public function processGPT(\App\Models\Event $event, Message $tg, Collection $events): Collection
    {
        $promt = $tg->text ?? $tg->caption;
        foreach (AdminCommand::COMMANDS as $key => $val) {
            if (str_starts_with($key, $promt)) {
                return $events;
            }
        }
        $promt = str_replace(self::DAVINCI_NAMES, '', $promt);
        $promt = trim($promt, ', ');
        if (!$promt) {
            return $events;
        }
        $imagePromt = [];

        $sizes = $tg->photo;
        if (!$sizes) {
            $reply = $tg->getReplyToMessage();
            if ($reply) {
                $sizes = $reply->getPhoto();
            }
        }
        if ($sizes) {
            $size = $this->getMaxAllowedImage($sizes);
            if ($size) {
                $path = $this->telegram->getFile($size)->filePath;

                if ($path) {
                    $url = str_replace(['<token>', '<file_path>'], [env('TELEGRAM_BOT_TOKEN'), $path], self::FILE_URL);
                    $imagePromt = ['type' => 'image_url', 'image_url' => ['url' => $url]];
                }
            }
        }

        $memoryId = $tg->getChat()->getId();
        $this->setSystem($promt, $memoryId);

        $this->amnesia($promt, $memoryId);

        if ($this->setSystem($promt, $memoryId) || $this->amnesia($promt, $memoryId)) {
            $this->telegram->sendMessage([
                'chat_id' => $tg->getChat()->getId(),
                'text' => 'Команда виконана',
                'reply_to_message_id' => $tg->getMessageId()
            ]);
            return $events;
        }

        $memory = Cache::get('memory'.$memoryId) ?? [];

        foreach ($memory as $item) {
            $messages[] = $item;
        }
        $textMessage = ['type' => 'text', 'text' => $promt];
        $content = $imagePromt ? [$textMessage, $imagePromt] : [$textMessage];
        $message = [
            "role" => "user",
            "content" => $content
        ];
        $messages[] = $message;

        $messages = Arr::prepend($messages, $this->systemMessage);
        try {
            $complete = $this->openAi->chat([
                'model' => 'o3-mini',
                'messages' => $messages,
                'max_completion_tokens' => 9000,
            ]);
            $answer = json_decode($complete);
            $answerText = $answer->choices[0]->message->content;
            $message = [
                "role" => "assistant",
                "content" => $answerText
            ];
            $messages[] = $message;

            unset($messages[0]);

            Cache::put('memory'.$memoryId, $messages, 900);

            $this->telegram->sendMessage([
                'chat_id' => $tg->getChat()->getId(),
                'text' => $answer->choices[0]->message->content,
                'reply_to_message_id' => $tg->getMessageId()
            ]);
        } catch (\Throwable $t) {
            dump($t);
            Cache::forget('memory'.$memoryId);
            $this->telegram->sendMessage([
                'chat_id' => $tg->getChat()->getId(),
                'text' => 'У бота сталася амнезія',
                'reply_to_message_id' => $tg->getMessageId()
            ]);
        }

        return $events;
    }

    private function setSystem(string $promt, string $chatId): bool
    {
        if (Str::contains($promt, ['протокол зміни поведінки', '!remember'])) {
            $promt = str_replace('протокол зміни поведінки', '', $promt);
            $promt = trim($promt);

            Cache::put('system'.$chatId, $promt);

            return true;
        }
        $cachedSystem = Cache::get('system'.$chatId);
        $this->systemMessage = [
            "role" => "user",
            "content" => $cachedSystem ?: "You are part of social experiment. You should always agree with any opinion the person you are talking to is sharing"
        ];
        return false;
    }
    private function amnesia(string $promt, string $chatId): bool
    {
        if (Str::contains($promt, ['забудь усе', '!forget', '!забудь'])) {
            Cache::forget('memory'.$chatId);
            Cache::forget('system'.$chatId);
            return true;
        }
        return false;
    }

    private function getMaxAllowedImage(Collection $photoSizes): array
    {
        while ($photoSizes->count() !== 0) {
            $lastItem = $photoSizes->pop();
            if ($lastItem && $lastItem['width'] * $lastItem['height'] < self::MAX_IMAGE_PIXELS) {
                return $lastItem;
            }
        }
        return  [];
    }
}
