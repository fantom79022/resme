<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Telegram\Bot\Objects\Message;

class Randomize implements EventHandler
{
    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        $weightsValues = explode(',', $params);
        $weightsValues = array_map(function ($val) {
            return (int)$val;
        }, $weightsValues);

        $sum = array_sum($weightsValues);

        $randomValue = random_int(1, $sum);

        $nextEvents = $events->values();

        $currentSum = 0;
        $found = false;
        foreach ($weightsValues as $index => $value) {
            if ($nextEvents->get($index)->parent_event_id != $event->id) {
                continue;
            }
            $currentSum += $value;
            if (!$found && ($currentSum >= $randomValue)) {
                $found = true;
                continue;
            }
            $nextEvents->forget($index);
        }
        return $nextEvents;
    }
}
