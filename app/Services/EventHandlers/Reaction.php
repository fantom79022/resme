<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Reaction implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;

    public function __construct(\App\Telegram\Api $telegram)
    {
        $this->telegram = $telegram;
    }

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {
        $params = $event->params;
        try {
            $this->telegram->sendReaction([
                'chat_id' => $message->getChat()->getId(),
                'reaction' => '[{"type": "emoji", "emoji": "'.$params.'"}]',
                "big" => true,
                'message_id' => $message->getMessageId()
            ]);
        } catch (\Exception $exception) {
            dump($exception);
        }
        return $events;
    }
}
