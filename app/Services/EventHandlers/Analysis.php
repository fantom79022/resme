<?php

namespace App\Services\EventHandlers;

use Illuminate\Support\Collection;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Analysis implements EventHandler
{
    /**
     * @var Api
     */
    private $telegram;
    private OpenAi $openAi;

    public function __construct(Api $telegram, OpenAi $openAi)
    {
        $this->telegram = $telegram;
        $this->openAi = $openAi;
    }

    public function process(\App\Models\Event $event, Message $message, Collection $events): Collection
    {

        $promt = $message->getReplyToMessage();
        dump($promt);

        try {
            $answer = $this->openAi->completion([
                'model' => 'text-curie-001	',
                'prompt' => $promt,
                'temperature' => 0.5,
                'max_tokens' => 1000,
                'frequency_penalty' => 0,
                'presence_penalty' => 0.6,
            ]);
            $answer = json_decode($answer);

            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => $answer->choices[0]->text,
                'reply_to_message_id' => $message->getMessageId()
            ]);
        } catch (\Throwable $t) {
            dump($t->getMessage());
        }
        dump($answer);

        return $events;
    }
}
