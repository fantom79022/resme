<?php

namespace App\Services\Administration;

use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class Reaction implements AdminCommand
{

    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        $replyMessage = $message->getReplyToMessage();
        if (!$replyMessage) {
            $tokens = explode(AdminCommand::COMMAND_DELIMITER, $args, 2);
            $trigger = Arr::get($tokens, 0);
            $answer = Arr::get($tokens, 1);
            $trigger = str_replace('\=', '=', $trigger);
            $type = 'reaction';
        } else {
            $trigger = $args;
            [$answer, $typeTrigger] = $this->typeService->getAnswerAndTypeFromMessage($replyMessage, true);
        }
        $type = 'reaction';


        if (!$trigger || !$answer || !$type) {
            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __('Usage: /add TRIGGER as reply to ANSWER, or /add TRIGGER = TEXT_REACTION'),
                'reply_to_message_id' => $message->getMessageId()
            ]);
            return;
        }

        $triggerModel = $this->trigger->firstOrCreate(['trigger' => $answer]);

        $existed = $triggerModel->events()->exists() ? 'existing' : 'new';

        $triggerModel->events()->create([
            'type' => $type,
            'params' => $trigger
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __(
                'Added new reaction to :existed trigger ":trigger"', [
                'existed' => $existed,
                'trigger' => $answer
            ]),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
