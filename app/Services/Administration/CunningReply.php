<?php

namespace App\Services\Administration;

use App\Models\Event;
use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class CunningReply implements AdminCommand
{

    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        if (!$this->isCunningLike($message)) {
            return;
        }
        /**
         * @var \Telegram\Bot\Objects\Message $replyMessage
         */
        $replyMessage = $message->getReplyToMessage();
        $eventModel = \App\Models\Message::where('message_id', $replyMessage->messageId)->first();
        if (!$eventModel) {
            return;
        }

        [$params, $type] = $this->typeService->getAnswerAndTypeFromMessage($message, true);
        $triggerModel = $this->trigger->firstOrCreate([
            'trigger' => $params
        ]);
        $existed = $triggerModel->events()->exists() ? 'existing' : 'new';

        $triggerModel->events()->create([
            'params' => $eventModel->reply_content,
            'type' => $eventModel->reply_type
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __(
                'Added new cunning event to :existed cunning trigger', [
                'existed' => $existed,
            ]),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }

    private function isCunningLike(Message $message): bool
    {
        return in_array($message->getReplyToMessage()?->getText(), ['/add', '/alias']);
    }
}
