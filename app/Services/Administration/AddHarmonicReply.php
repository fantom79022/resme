<?php

namespace App\Services\Administration;

use App\Models\Trigger;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Phalcon\Db;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

/**
 * @deprecated
 */
class AddHarmonicReply implements AdminCommand
{
    const HARMONIC_DELIMITER = '|';
    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;

    public function __construct(Api $telegram, Trigger $trigger)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        $harmonic = null;
        $replyMessage = $message->getReplyToMessage();
        if (!$replyMessage) {
            $tokens = explode(AdminCommand::COMMAND_DELIMITER, $args, 2);
            $trigger = Arr::get($tokens, 0);
            $harmonic = Arr::get($tokens, 1);
            $trigger = str_replace('\=', '=', $trigger);
        } else {
            $trigger = $args;
            $harmonic = $replyMessage->getText();
        }
        $replies = explode(self::HARMONIC_DELIMITER, $harmonic);

        foreach ($replies as &$reply) {
            $reply = ltrim($reply);
        }
        $countedReplies = array_count_values($replies);
        if (!$trigger || !$harmonic || count($countedReplies) < 2) {
            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __('Usage: /random TRIGGER = FIRST|SECOND|THIRD|... or /random TRIGGER as reply to "FIRST|SECOND|THIRD|..."'),
                'reply_to_message_id' => $message->getMessageId()
            ]);
            return;
        }
        $triggerModel = $this->trigger->firstOrCreate(['trigger' => $trigger]);
        $existed = $triggerModel->events()->exists() ? 'existing' : 'new';
        \Illuminate\Support\Facades\DB::transaction(function () use ($trigger, $countedReplies) {
            $triggerModel = $this->trigger->firstOrCreate(['trigger' => $trigger]);
            $triggerModel->events()->create([
                'type' => 'randomize',
                'params' => implode(',', array_values($countedReplies))
            ]);
            foreach ($countedReplies as $reply => $weight) {
                $triggerModel->events()->create([
                    'type' => $reply ? 'text' : 'blank',
                    'params' => $reply
                ]);
            }
        });
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __(
                'Added new event to :existed trigger ":trigger"', [
                'existed' => $existed,
                'trigger' => $trigger
            ]),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
