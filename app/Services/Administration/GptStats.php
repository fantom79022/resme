<?php

namespace App\Services\Administration;

use App\Models\Trigger;
use App\Models\UserGptRequest;
use App\Services\AnswerTypeFromMessage;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class GptStats implements AdminCommand
{

    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;

    private $userGptRequest;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService, UserGptRequest $userGptRequest)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
        $this->userGptRequest = $userGptRequest;

    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        $users = $this->userGptRequest->getActiveUsers();

        $list = '';
        foreach ($users as $user) {
            $list.= '@'.$user['name'] . ': ' . $user['count'] . PHP_EOL;
        }
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => $list,
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
