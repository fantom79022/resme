<?php

namespace App\Services\Administration;

use App\Models\Event;
use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Phalcon\Db;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class AddRandomReply implements AdminCommand
{
    const HARMONIC_DELIMITER = '|';
    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;
    private AnswerTypeFromMessage $typeService;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        try {
            $replyMessage = $message->getReplyToMessage();
            if (!$replyMessage) {
                $this->sendHelpMessage($message);
                return;
            } else {
                $tokens = explode(AdminCommand::COMMAND_DELIMITER, $args, 2);
                $trigger = Arr::get($tokens, 0);
                $weight = Arr::get($tokens, 1, 1);
                $trigger = str_replace('\=', '=', $trigger);
                if (!is_numeric($weight) && (int)$weight > 0) {
                    $this->sendHelpMessage($message);
                    return;
                }
            }
            [$trigger, $regexp] = Trigger::isRegexp($trigger);
            $trigger = trim($trigger);
            $triggerModel = Trigger::firstOrCreate(['trigger' => $trigger, 'regexp' => $regexp]);
            $event = $triggerModel->events()->where('type', Event::TYPE_RANDOMIZE)->first();
            $existingEvent = true;
            if (!$event) {
                $existingEvent = false;
                $event = $triggerModel->events()->create(['type' => Event::TYPE_RANDOMIZE, 'params' => $weight]);
            } else {
                $event->params = $event->params . ',' . $weight;
                $event->save();
            }

            [$answer, $type] = $this->typeService->getAnswerAndTypeFromMessage($replyMessage);
            $existed = $existingEvent ? 'existing' : 'new';

            if (!$type) {
                $this->sendHelpMessage($message);
            }
            $triggerModel->events()->create([
                'type' => $type,
                'params' => $answer,
                'parent_event_id' => $event->id
            ]);

            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __(
                    'Added new random event to :existed trigger ":trigger" with :weight weight', [
                    'existed' => $existed,
                    'trigger' => $trigger,
                    'weight' => $weight
                ]),
                'reply_to_message_id' => $message->getMessageId()
            ]);
        } catch (\Throwable $t) {
            Log::error($t->getMessage(), $t->getTrace());
        }
    }

    private function sendHelpMessage(Message $message)
    {
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __('Usage: /random TRIGGER = WEIGHT"'),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
