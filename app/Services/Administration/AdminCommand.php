<?php


namespace App\Services\Administration;


use Telegram\Bot\Objects\Message;

interface AdminCommand
{
    const COMMAND_DELIMITER = ' = ';

    const COMMANDS = [
        '/add' => AddShortReply::class,
        '/remove' => RemoveTrigger::class,
        '/random' => AddRandomReply::class,
        '/reaction' => Reaction::class,
        '/stats' => GptStats::class,
        '/alias' => Alias::class,
    ];

    public function process(?string $args, Message $message);
}
