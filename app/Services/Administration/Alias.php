<?php

namespace App\Services\Administration;

use App\Helpers\MessageHelper;
use App\Models\Event;
use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Illuminate\Support\Facades\Log;
use Phalcon\Db;
use App\Telegram\Api;
use Telegram\Bot\Objects\Message;

class Alias implements AdminCommand
{
    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;
    private AnswerTypeFromMessage $typeService;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        try {
            if (!$message->getReplyToMessage()) {
                $this->sendHelpMessage($message);
                return;
            }
            $messageContent = MessageHelper::getMessageContext($message->getReplyToMessage());

            if (!$args) {
                $replyMessageModel = \App\Models\Message::where('message_id', $message->messageId)->first();
                $replyMessageModel->reply_type = Event::TYPE_ALIAS;
                $replyMessageModel->reply_content = $messageContent;
                $replyMessageModel->save();

                $this->telegram->sendReaction([
                    'chat_id' => $message->getChat()->getId(),
                    'reaction' => '[{"type": "emoji", "emoji": "👀"}]',
                    "big" => true,
                    'message_id' => $message->getMessageId()
                ]);
                return;
            }

            [$alias, $regexp] = Trigger::isRegexp($args);

            $triggerModel = Trigger::firstOrCreate(['trigger' => $alias, 'regexp' => $regexp]);

            $existingEvent = $triggerModel->events()->where('type', Event::TYPE_ALIAS)->exists();
            $triggerModel->events()->create(['type' => Event::TYPE_ALIAS, 'params' => $messageContent]);
            $existed = $existingEvent ? 'existing' : 'new';

            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __(
                    'Added new expansion to :existed alias ":alias"', [
                    'existed' => $existed,
                    'alias' => $alias,
                ]),
                'reply_to_message_id' => $message->getMessageId()
            ]);
        } catch (\Throwable $t) {
            Log::error($t->getMessage(), $t->getTrace());
        }
    }

    private function sendHelpMessage(Message $message)
    {
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __('Normal guide: /alias ALIAS (as reply to the original command)
Cunning guide:
1. Reply with "/alias" without arguments to any non-bot message
2. Bot will react with 👀
3. Reply to "/alias" message with actual alias'),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
