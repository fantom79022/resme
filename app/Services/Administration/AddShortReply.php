<?php

namespace App\Services\Administration;

use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class AddShortReply implements AdminCommand
{

    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeService)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeService = $typeService;
    }

    //TODO: move message sending for handlers somewhere else
    public function process(?string $args, Message $message)
    {
        $replyMessage = $message->getReplyToMessage();
        if (!$replyMessage) {
            $tokens = explode(AdminCommand::COMMAND_DELIMITER, $args, 2);
            $trigger = Arr::get($tokens, 0);
            $answer = Arr::get($tokens, 1);
            $trigger = str_replace('\=', '=', $trigger);
            $type = 'text';
        } else {
            $trigger = $args;
            [$answer, $type] = $this->typeService->getAnswerAndTypeFromMessage($replyMessage);
        }


        if (!$trigger || !$answer || !$type) {
            if ($replyMessage) {
                $replyMessageModel = \App\Models\Message::where('message_id', $message->messageId)->first();
                [$response, $type] = $this->typeService->getAnswerAndTypeFromMessage($replyMessage);
                $replyMessageModel->reply_type = $type;
                $replyMessageModel->reply_content = $response;
                $replyMessageModel->save();
            }
            $this->telegram->sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text' => __('Usage: /add TRIGGER as reply to ANSWER,
                 Orr /add TRIGGER = TEXT_REACTION
                 Or reply trigger to the /add message'),
                'reply_to_message_id' => $message->getMessageId()
            ]);
            return;
        }
        [$trigger, $regexp] = Trigger::isRegexp($trigger);

        $triggerModel = $this->trigger->firstOrCreate(['trigger' => $trigger, 'regexp' => $regexp]);

        $existed = $triggerModel->events()->exists() ? 'existing' : 'new';

        $triggerModel->events()->create([
            'type' => $type,
            'params' => $answer
        ]);
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __(
                'Added new event to :existed trigger ":trigger"', [
                'existed' => $existed,
                'trigger' => $trigger
            ]),
            'reply_to_message_id' => $message->getMessageId()
        ]);
    }
}
