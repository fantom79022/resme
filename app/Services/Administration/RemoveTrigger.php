<?php

namespace App\Services\Administration;

use App\Models\Trigger;
use App\Services\AnswerTypeFromMessage;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class RemoveTrigger implements AdminCommand
{
    /**
     * @var Api
     */
    private $telegram;
    /**
     * @var Trigger
     */
    private $trigger;
    private AnswerTypeFromMessage $typeFromMessage;

    public function __construct(Api $telegram, Trigger $trigger, AnswerTypeFromMessage $typeFromMessage)
    {
        $this->telegram = $telegram;
        $this->trigger = $trigger;
        $this->typeFromMessage = $typeFromMessage;
    }

    public function process(?string $args, Message $message)
    {
        if (!$args) {
            $replyMessage = $message->getReplyToMessage();
            [$args, $type] = $this->typeFromMessage->getAnswerAndTypeFromMessage($replyMessage, true);
            if (!$args) {
                $this->telegram->sendMessage([
                    'chat_id' => $message->getChat()->getId(),
                    'text' => __('Usage: /remove TRIGGER'),
                    'reply_to_message_id' => $message->getMessageId(),
                ]);
                return;
            }
        }
        $removed = $this->trigger->removeTrigger($args) ? 'Removed!' : 'Trigger not found';
        $this->telegram->sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text' => __($removed),
            'reply_to_message_id' => $message->getMessageId(),
        ]);
    }
}
