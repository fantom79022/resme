<?php

namespace App\Services\AdminLogger;

use App\Telegram\Api;

class NotifyAdmin
{
    const CHANNEL = '-1001866695008';
    const THREAD_ID = 594;
    public function __construct(private Api $telegram)
    {

    }

    public function notify(string $message): void
    {
        $this->telegram->sendMessage([
            'chat_id' => self::CHANNEL,
            'message_thread_id' => self::THREAD_ID,
            'text' => $message
        ]);
    }
}
