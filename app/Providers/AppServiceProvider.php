<?php

namespace App\Providers;

use App\Models\Event;
use App\Models\Trigger;
use App\Services\EventHandlers\Administration\AddLoss;
use Illuminate\Support\ServiceProvider;
use Orhanerday\OpenAi\OpenAi;
use Telegram\Bot\Api;
use Telegram\Bot\Objects\Message;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(AddLoss::class)
            ->needs(Trigger::class)
            ->give(function () {
                return Event::where('type', 'loss')
                    ->first()
                    ->trigger()
                    ->firstOrFail();
            });

        $this->app->bind(Message::class, function ($app, $message) {
            return new Message($message);
        });

        $this->app->bind(OpenAi::class, fn() => new OpenAi(env('OPENAI_API_KEY')));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
