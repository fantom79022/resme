<?php

use App\Models\Client;
use App\Models\Trigger;
use Illuminate\Database\Seeder;

class ReactorSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        $phrases = [
            [1, 'что делает орда', 'сосет'],
            [2, 'что делает орда?', 'сосёт'],
            [3, 'Фантом', 'Дебил'],
            [4, 'Митя', 'Ебал я его в рот'],
            [5, 'ваш бот сломан?', 'хуеломан'],
            [6, 'ваш бот сломан', 'хуеломан'],
            [7, 'бот умер', 'мертвее тебя внутри?'],
            [8, 'бот сломался', 'анус твой сломался'],
            [9, 'бот упал', 'не так, как твои стандарты'],
            [10, 'серьезно?', 'да, серьезно'],
            [11, 'Кхм', 'будь здоров'],
            [13, 'с др', '+1'],
            [14, 'С др)', '+1'],
            [15, 'бот сломался?', 'Иди нахуй'],
            [16, 'Гордон', 'Гордон. Ладно, он уебок.'],
            [17, 'Глеб', 'Аутист'],
            [18, 'Тоша', 'Тоша'],
            [19, 'юмпер', '777'],
            [20, 'Хоши', 'ХОШИ!!!!!!!!!!!!!'],
            [21, 'Мила', 'Мяу'],
            [22, 'Херундий', 'Кто?'],
            [23, 'Кот', 'Только не кот!'],
            [24, 'Бот, ты охуел?', 'Нет, а ты?'],
            [25, 'С др, Фантик', 'Сам думай'],
            [26, 'О, бот сломался', 'анус твой сломался'],
            [27, 'тоша сломал бота', 'Тоша и тебя сейчас сломает'],
            [28, 'Ева, правила', 'Ты ебанутый?'],
            [29, 'Get-item()', 'хер тебе'],
            [30, 'Включаем режим лимитов', 'Да хоть бессмертия, рандом не щадит никого'],
            [31, '!хилл', 'хуилл!'],
            [32, 'Ева, одинокий вечер', 'https://ok.ru/video/336272886508'],
            [33, 'Одинокий вечер', 'https://www.pornhub.com'],
            [37, '!хил', 'хуил!'],
            [38, 'аутист', 'Глеб'],
            [39, 'Опппааа', 'Хуёппппааа'],
            [40, 'нера', 'Прости, Нера, нету на тебя никакой фразы'],
            [41, 'нерра', 'Прости, Нера, нету на тебя никакой фразы'],
            [42, 'Тоша гей', 'Нет'],
            [43, '!Тоша гей', 'Нет'],
            [44, 'АЛЬЯНС?', 'СОСАТБ'],
            [45, 'АЛЬЯНС!?', 'СОСАТБ'],
            [46, 'АЛЬЯНС', 'СОСАТБ'],
            [47, 'Это херундию повезло встретится с Митосом', 'Кому повезло?'],
            [48, 'Бот, статистика', 'Судя по статистике - ты пидор'],
            [49, 'Миллионы ответят', 'СОСАТЬ'],
            [50, 'БОЛЬШЕ НИКАКИХ РЕШЕТОК?', 'решетка'],
            [51, 'БОЛЬШЕ НИКАКИХ РЕШЕТОК', 'решетка'],
            [55, 'что делает альянс?', 'сосёт'],
            [56, 'к х м', 'б у д ь з д о р о в'],
            [57, 'Бот, спасибо', 'Всегда пожалуйста'],
            [58, 'спасибо, бот', 'Не за что'],
            [59, 'Сас', 'https://www.youtube.com/watch?v=znFTmidWhc4'],
            [61, 'Вика', 'Ты что, куришь?'],
            [62, 'Заплати ведьмаку', 'Чеканной монетой'],
            [63, 'Мразота ебаная', 'Глеб'],
            [64, 'Ведьмаку заплатите', 'Чеканной монетой'],
            [65, 'Ведьмаку заплати', 'Чеканной монетой'],
            [67, 'fantom', 'debil'],
            [68, 'kot', 'tol\'ko ne kot'],
            [69, 'khm', 'bud\' zdorov'],
            [70, 'cyka', 'blyat'],
            [75, 'сакральный олень', 'https://i.imgur.com/JeWSeNW.jpg'],
            [75, 'мортдог', 'гнида'],
            [75, 'juan', 'https://www.youtube.com/watch?v=H9aC5AGY9YU'],
            [75, 'федя', 'як же я ненавиджу федю'],
            [75, 'Слава Україні', 'Героям слава'],
            [75, '/count', 'Мало'],
            [77, 'bts', 'AgACAgIAAx0CSEVAUQACTTBi4lizvvc_6NlwPJPB89JqaEvm5gAChLgxG2iG8Ur-crBMnCkUogEAAwIAA20AAykE', false, 'photo'],
            [77, 'федя', 'CAACAgIAAx0CSEVAUQABAR0IYuJYjw-ppTCUolJc6euNW1R_LzMAAnQZAALpUQhLhwzyHU5hTswpBA', false, 'sticker'],
            [77, 'яре яре', 'CAACAgIAAxkBAAIEEGLiZ6Jh7oJvRFG3nzv88Mu0n2u9AAKKAAP0Dg0El0GFZ9lsCBApBA', false, 'sticker'],
            [77, 'яре яре', 'CAACAgIAAxkBAAIEEGLiZ6Jh7oJvRFG3nzv88Mu0n2u9AAKKAAP0Dg0El0GFZ9lsCBApBA', false, 'sticker'],
            [77, 'луганщина', 'ПИСНЯ МОЯ'],
            [77, 'буряты мы', 'я русская'],
            [77, 'пойдем выпьем чаю', 'я проиграл'],
            [77, 'he', 'https://www.youtube.com/watch?v=X8avbciUP3c'],
            [77, 'сирена', '‼️‼️ ВИУ  ВИУ  ВИУ ‼️‼️'],
        ];

        $randoms = [
            "Майор" => [
                'weights' => '1,1',
                'answers' => [
                    ['Тикает', 'text'],
                    ['Нервничает', "text"]
                ],
                'regexp' => false
            ],
            "ооо" => [
                'weights' => '1,1',
                'answers' => [
                    ['моя оборона', 'text'],
                    ['пошли вы все нахуй', "text"]
                ],
                'regexp' => false
            ],
            "да" => [
                'weights' => '50,1',
                'answers' => [
                    ['', 'blank'],
                    ['манда', "text"]
                ],
                'regexp' => false
            ],
            "сука" => [
                'weights' => '100,1',
                'answers' => [
                    ['', 'blank'],
                    ['манда', "text"]
                ],
                'regexp' => false
            ],
            "*русская рулетка*" => [
                'weights' => '1,4',
                'answers' => [
                    ['победа', 'text'],
                    ['не победа', 'text'],
                ],
                'regexp' => false
            ],
            ".*дисл.*" => [
                'weights' => '3,1',
                'answers' => [
                    ['CAACAgIAAxkBAAKBj2LhZ1UxOPTh7x6u7QSIiIUF5KNZAAL-AgACXEuACbacLm8J3SO0KQQ', 'sticker'],
                    ['CAACAgIAAxkBAAKBkGLhZ19CpFKj3TymdEJZqRCEpC_oAAK0AwACXEuACdXbOraDItsEKQQ', 'sticker'],
                ],
                'regexp' => true
            ],
            "а" => [
                'weights' => '2,1',
                'answers' => [
                    ['https://www.youtube.com/watch?v=vYxbN9xIITQ', 'text'],
                    ['https://youtu.be/cQiE6kXgMuc', 'text'],
                ],
                'regexp' => false
            ],

            "." => [
                'weights' => '1000,1',
                'answers' => [
                    ['', 'blank'],
                    ['AgACAgIAAxkBAAKBwWLha_uZn1yhof5uHRubJ16XLWGtAAJvwTEbcXIIS2konBU5oW7TAQADAgADbQADKQQ', 'photo'],
                ],
                'regexp' => true
            ],
            "ясно" => [
                'weights' => '1,1,1,1',
                'answers' => [
                    ['Я тоже вижу прекрасный сон', 'text'],
                    ['Понятно всё', 'text'],
                    ['AgACAgIAAxkDAAOuYuFz7NsuvadOifx7fr9kPBppOPkAAserMRsyRZlJ1OR1JntLgyABAAMCAAN5AAMpBA', 'photo'],
                    ['Солнечно', 'text'],
                ],
                'regexp' => true
            ],
            "сакральный олень" => [
                'weights' => '1,1,1,1,1,1,1,1,1,1',
                'answers' => [
                    ['0', 'text'],
                    ['1', 'text'],
                    ['2', 'text'],
                    ['3', 'text'],
                    ['4', 'text'],
                    ['5', 'text'],
                    ['6', 'text'],
                    ['7', 'text'],
                    ['8', 'text'],
                    ['9', 'text'],
                ],
                'regexp' => true
            ],

        ];
        $losses = [
            'AgACAgQAAxkBAAOvYuJZvs5dJct_os_ZxdZ9Lt1OGAQAAsOnMRsYDIRTqo_R8PxkVggBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAOwYuJZw-phBP73W3bV7V3yN9eGESYAAs6nMRsBHo1Tv9KtuduKwX8BAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAOxYuJaIbVQ1Rl0gTjRm_rxjGDARt0AAvSnMRuTm61TUxM17BAfDM4BAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAOyYuJaPB2tTgbXtBu92_RqsyGbHqAAAs2nMRuLzqxTKcVQxyvwbCUBAAMCAAN4AAMpBA',
            'AgACAgIAAxkBAAOzYuJaU123j_uWs4voEsOF4RzB7eUAAompMRsWv9lLFIn-m9DfZnwBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO0YuJaZnQGha5G0JOInZHOkEm0oF8AAuunMRtoiNxT8ChimrsMAWkBAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO1YuJadzxZTDJ8DQFygixxwQjkftMAArKnMRswjvxT4hh0Vgl1p7ABAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO2YuJajVsYDp8PZUPiD9qEYFxgsJkAAsWnMRsH8_xT50_gH6sclVUBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO3YuJapWyEmQtDk5QLBO8GB0uESjYAAsOnMRuWi5VQxfHkls22tvcBAAMCAAN5AAMpBA',
            'AgACAgIAAxkBAAO4YuJavQfgbD0tKAeya1TIYsDDfUAAAmKpMRtlSJlI2PjHIoxSEIgBAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO5YuJa4xdpopw8BSjYTWHiv0zgj1cAArGnMRuufQVR2GTTEnBIrgoBAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO6YuJa9qGonuLctw0OfLuzTtYMj-kAArSnMRtHezxRI6y0rhtnqHQBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO7YuJbE0SWQGVZlmn8C2cWBaQoYlMAAt-nMRuwVc1Sa2cC4ytCntIBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO8YuJbFxdsdvLnB21LY2HKV-QOq8EAAt2nMRtI9n1TjAABhECYgE0ZAQADAgADeAADKQQ',
            'AgACAgQAAxkBAAO9YuJbG4aJY1S4-EhVbFW7M-npmbIAAsCnMRs805RR4_ng4VXPugkBAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO-YuJbH5rWQEFb1W-4InUHdNILloEAAtGnMRso3ZRR8jQWnkLv9zABAAMCAAN3AAMpBA',
            'AgACAgQAAxkBAAO_YuJbI-bFM73Bxagv_L7tMEDsHBgAAuinMRuOAAGUUXGEGViFv_RYAQADAgADeAADKQQ',
            'AgACAgQAAxkBAAPAYuJbJuykSH1SPjjWEAuduXTJzXUAAiGoMRvTdKxSlje2CQ165M8BAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAPBYuJbLWlSKj6W1aY217MROBr9RNsAAsKnMRuxX9xQ4qaKiGaYLuMBAAMCAAN4AAMpBA',
            'AgACAgIAAxkBAAPCYuJbMNmEKSg1Yi2mn2GaoOUO5J8AArSrMRtUJwhLzWfq0M20888BAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAPDYuJbNQPZJQwuwcH98b1VJoTifnYAAqA3OxsWHWQHyEaOoNFEDJkBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAPEYuJbeXb1bwb3BrXN2SgDWK0urEMAArunMRuXUAVRoa-ZQHrKbncBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAPFYuJbfbzFl0KxcZx47nl6HUkvOVgAAj6oMRvjOU1SP8jgF3tFRtMBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAOvYuJZvs5dJct_os_ZxdZ9Lt1OGAQAAsOnMRsYDIRTqo_R8PxkVggBAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAOwYuJZw-phBP73W3bV7V3yN9eGESYAAs6nMRsBHo1Tv9KtuduKwX8BAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAOxYuJaIbVQ1Rl0gTjRm_rxjGDARt0AAvSnMRuTm61TUxM17BAfDM4BAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAOyYuJaPB2tTgbXtBu92_RqsyGbHqAAAs2nMRuLzqxTKcVQxyvwbCUBAAMCAAN5AAMpBA',
            'AgACAgIAAxkBAAOzYuJaU123j_uWs4voEsOF4RzB7eUAAompMRsWv9lLFIn-m9DfZnwBAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAO0YuJaZnQGha5G0JOInZHOkEm0oF8AAuunMRtoiNxT8ChimrsMAWkBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO1YuJadzxZTDJ8DQFygixxwQjkftMAArKnMRswjvxT4hh0Vgl1p7ABAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAO2YuJajVsYDp8PZUPiD9qEYFxgsJkAAsWnMRsH8_xT50_gH6sclVUBAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAO3YuJapWyEmQtDk5QLBO8GB0uESjYAAsOnMRuWi5VQxfHkls22tvcBAAMCAAN4AAMpBA',
            'AgACAgIAAxkBAAO4YuJavQfgbD0tKAeya1TIYsDDfUAAAmKpMRtlSJlI2PjHIoxSEIgBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO5YuJa4xdpopw8BSjYTWHiv0zgj1cAArGnMRuufQVR2GTTEnBIrgoBAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAO6YuJa9qGonuLctw0OfLuzTtYMj-kAArSnMRtHezxRI6y0rhtnqHQBAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO-YuJbH5rWQEFb1W-4InUHdNILloEAAtGnMRso3ZRR8jQWnkLv9zABAAMCAAN5AAMpBA',
            'AgACAgQAAxkBAAO_YuJbI-bFM73Bxagv_L7tMEDsHBgAAuinMRuOAAGUUXGEGViFv_RYAQADAgADbQADKQQ',
            'AgACAgQAAxkBAAPBYuJbLWlSKj6W1aY217MROBr9RNsAAsKnMRuxX9xQ4qaKiGaYLuMBAAMCAANtAAMpBA',
            'AgACAgIAAxkBAAPCYuJbMNmEKSg1Yi2mn2GaoOUO5J8AArSrMRtUJwhLzWfq0M20888BAAMCAAN4AAMpBA',
            'AgACAgQAAxkBAAPDYuJbNQPZJQwuwcH98b1VJoTifnYAAqA3OxsWHWQHyEaOoNFEDJkBAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAPEYuJbeXb1bwb3BrXN2SgDWK0urEMAArunMRuXUAVRoa-ZQHrKbncBAAMCAANtAAMpBA',
            'AgACAgQAAxkBAAPFYuJbfbzFl0KxcZx47nl6HUkvOVgAAj6oMRvjOU1SP8jgF3tFRtMBAAMCAANtAAMpBA',
            'AgACAgIAAxkBAAKCv2LiZ-4ggrIPnq1HMXLEkOQk4uzEAAKBwzEbv4IIS3NR6N334sJUAQADAgADeQADKQQ',
            'AgACAgIAAxkDAAJz_GLiZ_bCGQU7c5FXo6bCWSS87U_TAAL5sDEbuQsAAUpY6gy22GnHswEAAwIAA3kAAykE',
            'AgACAgQAAxkDAAJz-mLiZ_wkZwdIQiVJ_Y1uT-Iuu5c6AAKxpzEbrn0FUdhk0xJwSK4KAQADAgADeQADKQQ',
            'AgACAgQAAxkDAAJz-GLiZ__KFYwNqoyi6md58JVmrOO5AALDpzEblouVUMXx5JbNtrb3AQADAgADeQADKQQ',
            'AgACAgIAAxkDAAJz9mLiaAIT2V7mjOhqXVfnw7cfOyknAAJPrjEbwbjJSxzz1JvzcZuaAQADAgADeQADKQQ',
            'AgACAgQAAxkDAAJz9GLiaAdURzW6F8yC1zw-qH98q55YAALopzEbjgABlFFxhBlYhb_0WAEAAwIAA3gAAykE',
            'AgACAgIAAxkDAAJz8GLiaAsEd5nuVJRNQdCt1zZjK4ZMAAKdtTEb8iPoSW50bVoF7N2_AQADAgADeQADKQQ',
            'AgACAgIAAxkBAAJz6mLiaA7NmnVm9YaWYHVW5OPPBG9rAAJJtzEbV9epSlG1VMrYAmxGAQADAgADeAADKQQ',
            'AgACAgIAAxkBAAJzYmLiaBKx2KVPjJIoaTd3D7MGOxj8AALttzEbyWQwSudDGSd1cRt9AQADAgADbQADKQQ',
            'AgACAgIAAxkBAAJi_WLiaBcoO7DNonW4QhXmNmt_Y4pDAAJFuDEbCpoZSgdDAU0rA-IHAQADAgADeQADKQQ',
            'AgACAgIAAxkBAAJi-mLiaBtKkKlpXUX9vzAeMOAajXvMAALwtzEbCpoZSq7-v_gJfKJxAQADAgADeQADKQQ',
            'AgACAgIAAxkBAAJhAWLiaB5ctXj_Bei0Fv8hP9DJAsizAAJisjEb9U65SRmSH_VmJRQAAQEAAwIAA3gAAykE',
            'AgACAgIAAxkBAAJIqmLiaCGkP-ukeotGcoXYgq4zDtCTAAJ-szEbUaGBSGvYuyb-YEu0AQADAgADeQADKQQ',
            'AgACAgIAAxkBAAJHfmLiaCZyIRz2BGP0MuhZmDkggCVFAAKJszEbJ344S0ybZuByuz-TAQADAgADeAADKQQ'
        ];

        foreach ($phrases as $phrase) {
            $trigger = Trigger::UpdateOrCreate(['trigger' => $phrase[1], 'regexp' => isset($phrase[3]) && $phrase[3]]);
            $type = $phrase[4] ?? 'text';
            $trigger->events()->updateOrCreate(['params' => $phrase[2], 'type' => $type]);
        }

        foreach ($randoms as $trigger => $data) {
            $trigger = Trigger::UpdateOrCreate(['trigger' => $trigger, 'regexp' => $data['regexp']]);


            $trigger->events()->updateOrCreate(['params' => $data['weights'], 'type' => 'random']);

            foreach ($data['answers'] as $answer) {
                $trigger->events()->updateOrCreate(['params' => $answer[0],'type' => $answer[1]]);
            }
        }
        $addLoss = Trigger::UpdateOrCreate(['trigger' => "/loss", 'regexp' => false]);
        $addLoss->events()->updateOrCreate(['params' => '', 'type' => 'add-loss']);

        $loss = Trigger::UpdateOrCreate(['trigger' => "можно лосс?", 'regexp' => false]);
        $loss->events()->updateOrCreate(['params' => '', 'type' => 'loss']);
        $loss->events()->updateOrCreate(['params' => 'нельзя', 'type' => 'text']);
        foreach ($losses as $lossImg) {
            $loss->events()->updateOrCreate(['params' => $lossImg, 'type' => 'photo']);

        }

        Client::updateOrCreate(['user_id' => 73060171, 'chat_id' => null, 'role' => 'admin']);
        Client::updateOrCreate(['user_id' => 311962894, 'chat_id' => null, 'role' => 'admin']);
        Client::updateOrCreate(['user_id' => 203166081, 'chat_id' => null, 'role' => 'admin']);
        Client::updateOrCreate(['user_id' => 67964660, 'chat_id' => null, 'role' => 'admin']);
    }
}
