<?php

use App\Models\Client;
use App\Models\Trigger;
use Illuminate\Database\Seeder;

class OpenAirSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        $davinciNames = \App\Services\EventHandlers\Davinci::DAVINCI_NAMES;
        dd($davinciNames);
        Trigger::create();
    }
}
