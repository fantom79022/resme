<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table){
            $table->bigInteger('message_id')->unsigned()->default(null)->nullable();
            $table->string('reply_content')->default(null)->nullable();;
            $table->string('reply_type')->default(null)->nullable();

            $table->index(['message_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table){
            $table->dropColumn('message_id');
        });
    }
}
