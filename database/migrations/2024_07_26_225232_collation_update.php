<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CollationUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter table triggers    modify `trigger` varchar(4096) collate utf8mb4_0900_ai_ci not null;");
        DB::statement("alter table events    modify `params` varchar(4096) collate utf8mb4_0900_ai_ci not null;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
